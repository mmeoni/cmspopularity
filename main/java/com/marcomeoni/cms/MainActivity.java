package com.marcomeoni.cms;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.marcomeoni.cms.db.Site;
import com.marcomeoni.cms.db.SiteDAO;
import com.marcomeoni.cms.fragments.FragmentHTTP;
import com.marcomeoni.cms.fragments.FragmentSites;
import com.marcomeoni.cms.fragments.FragmentWebview;
import com.marcomeoni.cms.utility.CMSMapView;
import com.marcomeoni.cms.utility.ConnectionBroadReceiver;
import com.marcomeoni.cms.utility.MessageBox;

import java.util.ArrayList;

public class MainActivity extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnMapReadyCallback {
    private static final String TAG = MainActivity.class.getSimpleName();
    private String[] mMenuNames;

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private int mSelectedDrawerIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMenuNames = getResources().getStringArray(R.array.menus);

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mNavigationDrawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        mSelectedDrawerIndex = position;
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = null;

        switch (position) {
            case 0:
                break;
            case 1:
            case 2:
                // TODO: if no network maps is not drawn!
                MapFragment mapFragment = MapFragment.newInstance(); //can call kinda getMapOptions(position) to set selection-specific options;
                mapFragment.getMapAsync(this);
                Log.d("CMS Popularity", TAG + ".onNavigationDrawerItemSelected(). Open " + mapFragment.toString() + " -> CMSMapView");
                fragmentManager.beginTransaction().replace(R.id.container, mapFragment).commit();
                break;
            case 3:
                // TODO: NewtworkReceiver not fired?? Have to check network manually
                //registerReceiver(network, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                //if (!network.isConnected(this)) {
                //    MessageBox.Ok(this, "Network Error", "Internet connection not available!", android.R.drawable.ic_dialog_alert);
                //    return;
                //}
                ConnectionBroadReceiver network = new ConnectionBroadReceiver();
                switch (network.networkActiveType(this.getApplicationContext())) {
                    case 0:
                        MessageBox.Ok(this, "Network Error", "Internet connection unavailable!", android.R.drawable.ic_dialog_alert);
                        return;
                    case 3:
                        SharedPreferences sp = this.getApplicationContext().getSharedPreferences(getString(R.string.PREFERENCE_FILENAME), MODE_PRIVATE);
                        if (!sp.getBoolean(getString(R.string.PREFERENCE_NETWORK_3G_DOWNLOAD), false)) {
                            MessageBox.Ok(this, "You are on cellular network", "Enable cellular download in app settings", android.R.drawable.ic_dialog_alert);
                            return;
                        }
                }

                // Retrieve configured (active) URLs from DB
                SiteDAO siteDAO = new SiteDAO(this.getApplicationContext());
                ArrayList<Site> sites = siteDAO.retrieveSites(false);
                ArrayList<String> siteURLs = new ArrayList<String>();
                for (Site site: sites) {
                    siteURLs.add(site.getUrl());
                }

                // Fetch data from the Internet
                fragment = new FragmentHTTP();
                Bundle bundle = new Bundle();
                bundle.putSerializable("URLs", siteURLs);
                fragment.setArguments(bundle);
                break;
            case 4:
                fragment = new FragmentSites();
                break;
            case 5:
                fragment = new FragmentWebview();
                // If you'd want to start a new Activity:
                // Intent intent = new Intent(this, SiteMainActivity.class);
                // intent.putExtra("From_ActivityMain", "hello SiteMainActivity");
                // startActivity(intent);
                break;
        }

        if (fragment != null) {
            Log.d("CMS Popularity", TAG + ".onNavigationDrawerItemSelected(). Fragment " + fragment.toString() + " created");
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }

        restoreActionBar(mMenuNames[mSelectedDrawerIndex]);
    }

    public void restoreActionBar(CharSequence title) {
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(android.R.color.transparent);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // TODO: Memory leak?? better static??
        CMSMapView map = new CMSMapView(getApplicationContext(), this, googleMap, mSelectedDrawerIndex);
    }

}