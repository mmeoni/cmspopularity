package com.marcomeoni.cms;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.marcomeoni.cms.adapter.DrawerAdapter;
import com.marcomeoni.cms.adapter.DrawerItem;
import com.marcomeoni.cms.fragments.FragmentPreferences;
import com.marcomeoni.cms.fragments.FragmentSitesAdd;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.androidfragmentId.com/design/patterns/navigation-drawer.html#Interaction"> design guidelines</a>
 */
public class NavigationDrawerFragment extends Fragment {
    private static final String TAG = NavigationDrawerFragment.class.getSimpleName();
    private static final String STATE_SELECTED_POSITION = "STATE_SELECTED_POSITION";
    private static final String PREF_DRAWER_OPEN = "PREF_DRAWER_OPEN";

    private NavigationDrawerCallbacks mCallbacks;
    // Helper component that ties the action bar to the navigation drawer
    private ActionBarDrawerToggle mDrawerToggle;

    private Activity activity;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 1;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    private String[] mDrawerNames;
    private TypedArray mDrawerIcons;
    private List<DrawerItem> mDrawerItems;
    private DrawerAdapter mDrawerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        activity = getActivity();

        // Preference flag indicating whether or not the user knows a drawer exists
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        mUserLearnedDrawer = sp.getBoolean(PREF_DRAWER_OPEN, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }

        // Select either the default item (1) or the last selected
        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mDrawerListView = (ListView) inflater.inflate(R.layout.navigation_drawer_listview, container, false);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });

        // Prepare the data source for the custom Adapter
        mDrawerIcons = getResources().obtainTypedArray(R.array.icons);
        mDrawerNames = getResources().getStringArray(R.array.menus);
        mDrawerItems = new ArrayList<DrawerItem>();
        for (int i = 0; i < mDrawerNames.length; i++) {
            DrawerItem item = new DrawerItem(mDrawerNames[i], mDrawerIcons.getResourceId(i, -1));
            mDrawerItems.add(item);
        }

        // Instantiate a custom Adapter that inflates R.layout.list_item_drawer
        mDrawerIcons.recycle();
        mDrawerAdapter = new DrawerAdapter(activity.getApplicationContext(), mDrawerItems);
        mDrawerListView.setAdapter(mDrawerAdapter);
        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
        return mDrawerListView;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions
     *
     * @param fragmentId   - The android:id of this fragment in its activity's layout
     * @param drawerLayout - The DrawerLayout containing this fragment's UI
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = activity.findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // set up the drawer's list view with items and click listener
        ActionBar actionBar = activity.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the interactions between the nav drawer and the action bar app icon.
        //
        // mDrawerToggle = new ActionBarDrawerToggle( /* this is for v4 */
        //        activity,                   /* host Activity */
        //        mDrawerLayout,                   /* DrawerLayout object */
        //        R.drawable.ic_drawer,            /* nav drawer image to replace 'Up' caret */
        //        R.string.navigation_drawer_open,        /* "open drawer" description */
        //        R.string.navigation_drawer_close /* "close drawer" description */) {
        mDrawerToggle = new ActionBarDrawerToggle(activity, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }
                // triggers onPrepareOptionsMenu()
                activity.invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // user manually opened the drawer; store this flag to prevent auto-showing the nav drawer in the future
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
                    sp.edit().putBoolean(PREF_DRAWER_OPEN, true).apply();
                }
                // triggers onPrepareOptionsMenu()
                activity.invalidateOptionsMenu();
            }
        };

        // Drawer not opened yet, open it
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Show app actions in the action bar - use && isDrawerOpen() if only when drawer open -
        // showGlobalContextActionBar controls the top-left area of the action bar
        if (mDrawerLayout != null) {
            inflater.inflate(R.menu.main, menu);
            ActionBar actionBar = activity.getActionBar();
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            //actionBar.setTitle(R.string.app_name);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        switch (mCurrentSelectedPosition) {
            case 4:
                menu.findItem(R.id.action_addsite).setVisible(true);
                break;
            default:
                menu.findItem(R.id.action_settings).setVisible(true);
                menu.findItem(R.id.action_addsite).setVisible(false);
        }

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // keep otherwise drawer doesnt toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.action_settings:
                fragment = new FragmentPreferences();
                break;
            case R.id.action_addsite:
                fragment = new FragmentSitesAdd();
                break;
        }

        if (fragment != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack("FRAGMENT") // If string is null fragments overlap!
                    .commit();

            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawer(mFragmentContainerView);
            }

            return true;
        }

        Log.d("CMS Popularity", TAG + ".onOptionsItemSelected(). Error creating fragment");
        return super.onOptionsItemSelected(item);
    }

    /**
     * Callbacks interface that all activities using this fragment must implement
     */
    public interface NavigationDrawerCallbacks {
        // Called when an item in the navigation drawer is selected
        void onNavigationDrawerItemSelected(int position);
    }
}
