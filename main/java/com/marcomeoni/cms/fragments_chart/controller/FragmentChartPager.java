package com.marcomeoni.cms.fragments_chart.controller;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartItem;
import com.marcomeoni.cms.fragments_chart.model.ChartData;

/**
 * Fragment to show images in a horizontally swiping pager, allowing the user to select charts
 *
 * @author Marco Meoni
 */
public class FragmentChartPager extends Fragment implements OnPageChangeListener, ChartSelector {
    private static final String TAG = FragmentChartPager.class.getSimpleName();

    private ViewPager mViewPager;
    private ChartPagerAdapter mChartPagerAdapter;
    private OnChartSelectedListener mParentOnChartSelectedListener;
    private ChartItem[] mPagerChartItems;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("CMS Popularity", TAG + ".onAttach()");

        // Check if parent fragment (if there is one) implements the chart selection interface
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null && parentFragment instanceof OnChartSelectedListener) {
            mParentOnChartSelectedListener = (OnChartSelectedListener) parentFragment;
            mParentOnChartSelectedListener = (OnChartSelectedListener) parentFragment;
        }
        // Otherwise, check if parent activity implements the chart selection interface
        else if (activity != null && activity instanceof OnChartSelectedListener) {
            mParentOnChartSelectedListener = (OnChartSelectedListener) activity;
        }
        // If neither implements the chart selection callback, warn that selections are being missed
        else if (mParentOnChartSelectedListener == null) {
            Log.d("CMS Popularity", TAG + ".onAttach(): parent fragment/activity doesnt implement OnChartSelectedListener, "
                    + "image selections will not be communicated to other components!");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onCreate(): savedInstanceState " + (savedInstanceState == null ? "==" : "!=") + " null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("CMS Popularity", TAG + ".onCreateView(): savedInstanceState " + (savedInstanceState == null ? "==" : "!=") + " null");

        // Inflate the fragment main view in the container provided
        View v = inflater.inflate(R.layout.fragment_chart_pager, container, false);

        mViewPager = (ViewPager) v.findViewById(R.id.chart_pager_viewpager);
        mPagerChartItems = ChartData.getChartItems();

        mChartPagerAdapter = new ChartPagerAdapter(mPagerChartItems);
        mViewPager.setAdapter(mChartPagerAdapter);

        // Listen for page changes to update other views
        mViewPager.setOnPageChangeListener(this);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onViewCreated()");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onActivityCreated()");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onViewStateRestored()");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("CMS Popularity", TAG + ".onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("CMS Popularity", TAG + ".onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("CMS Popularity", TAG + ".onPause()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("CMS Popularity", TAG + ".onSaveInstanceState()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("CMS Popularity", TAG + ".onStop()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("CMS Popularity", TAG + ".onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("CMS Popularity", TAG + ".onDestroy()");
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Log.d("CMS Popularity", TAG + ".onPageSelected(): " + position);

        // Inform our parent listener that an image was selected
        if (mParentOnChartSelectedListener != null) {
            mParentOnChartSelectedListener.onChartSelected(mPagerChartItems[position], position);
        }
    }

    @Override
    public void setChartSelected(ChartItem imageItem, int position) {
        Log.d("CMS Popularity", TAG + ".setChartSelected(): title = " + imageItem.getTitle() + " position = " + position);
        if (isResumed()) {
            // If the selected position is valid, and different than what is
            // currently selected, move the pager to that image
            if (position >= 0 && position < mChartPagerAdapter.getCount()
                    && position != mViewPager.getCurrentItem()) {
                // Move the view pager to the current image
                mViewPager.setCurrentItem(position, true);
            }
        }
    }

}
