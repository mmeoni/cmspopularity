package com.marcomeoni.cms.fragments_chart.controller;

import com.marcomeoni.cms.fragments_chart.model.ChartItem;

/**
 * Listener interface to communicate chart selections.
 *
 * @author Marco Meoni
 */
public interface OnChartSelectedListener {

    /**
     * Inform the listener that an chart has been selected.
     *
     * @param chartItem
     * @param position
     */
    public void onChartSelected(ChartItem chartItem, int position);
}
