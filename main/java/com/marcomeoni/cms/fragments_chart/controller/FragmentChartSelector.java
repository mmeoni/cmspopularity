package com.marcomeoni.cms.fragments_chart.controller;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartData;
import com.marcomeoni.cms.fragments_chart.model.ChartItem;

/**
 * Fragment that allows the user to select a chart using various sub-fragments,
 * while keeping them in sync with each other
 * 
 * @author Marco Meoni
 */
public class FragmentChartSelector extends Fragment implements OnChartSelectedListener {
	private static final String TAG = FragmentChartSelector.class.getSimpleName();

	// State keys
    private static final String KEY_STATE_CUR_IMAGE_RESOURCE_ID = "KEY_STATE_CUR_IMAGE_RESOURCE_ID";
	private static final String KEY_STATE_CUR_IMAGE_POSITION = "KEY_STATE_CUR_IMAGE_POSITION";
	private static final String KEY_STATE_CUR_SITENAME = "KEY_STATE_CUR_SITENAME";
	private static final String KEY_STATE_CUR_POSITION = "KEY_STATE_CUR_POSITION";

	private String mSiteName = "";
	private Boolean mInitialCreate;
	private int mCurImageResourceId, mCurImagePosition;
	private ViewGroup mChartListLayout, mChartPagerLayout, mChartContainerLayout;
	private OnChartSelectedListener mParentOnChartSelectedListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Log.d("CMS Popularity", TAG + ".onAttach()");

		// Check if parent fragment (if there is one) implements the chart selection interface
		Fragment parentFragment = getParentFragment();
		if (parentFragment != null)
			Log.d("CMS Popularity", TAG + ".onAttach(): Parent fragment is " + parentFragment.toString());
		else
			Log.d("CMS Popularity", TAG + ".onAttach(): Parent fragment is NULL");

		if (parentFragment != null && parentFragment instanceof OnChartSelectedListener) {
			mParentOnChartSelectedListener = (OnChartSelectedListener) parentFragment;
		}
		// Otherwise, check if parent activity implements the image selection interface
		else if (activity != null && activity instanceof OnChartSelectedListener) {
			mParentOnChartSelectedListener = (OnChartSelectedListener) activity;
		}
		// If neither implements the image selection callback, warn that selections are being missed
		else if (mParentOnChartSelectedListener == null) {
			Log.d("CMS Popularity", TAG + ".onAttach(): parent fragment/activity doesnt implement OnChartSelectedListener, "
					+ "image selections will not be communicated to other components!");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("CMS Popularity", TAG + ".onCreate(): savedInstanceState " + (savedInstanceState == null ? "==" : "!=") + " null");

		// Restore state
		if (savedInstanceState != null) {
			mInitialCreate = false;

			mCurImageResourceId = savedInstanceState.getInt(KEY_STATE_CUR_IMAGE_RESOURCE_ID);
			mCurImagePosition = savedInstanceState.getInt(KEY_STATE_CUR_IMAGE_POSITION);
			mSiteName = savedInstanceState.getString(KEY_STATE_CUR_SITENAME);
		}
		// Otherwise, default state
		else {
			mInitialCreate = true;

			mCurImagePosition = ChartData.DEFAULT_CHART_INDEX;
			mCurImageResourceId = ChartData.getChartItems()[mCurImagePosition].getImageResId();

			Bundle bundle = getArguments();
			if (bundle != null)
				if (bundle.getString(KEY_STATE_CUR_SITENAME) != null)
					mSiteName = bundle.getString(KEY_STATE_CUR_SITENAME);
		}

		Log.d("CMS Popularity", TAG + ".onCreate(): mCurImagePosition=" + mCurImagePosition + ", sitename=" + mSiteName);

		// Set that this fragment has a menu
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_chart_selector, container, false);
		mChartContainerLayout = container;  // it's going to be fragment_chart_main

		Log.d("CMS Popularity", ">>>>" + TAG + ".onCreateView(): mChartContainerLayout is " + mChartContainerLayout.getClass().getSimpleName());

		// If this is the first creation of the fragment, add child fragments
		if (mInitialCreate) {
			mInitialCreate = false;

			FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();

			mChartListLayout = (ViewGroup) v.findViewById(R.id.chart_selector_list_container);
			if (mChartListLayout != null) {
				Log.d("CMS Popularity", TAG + ".onCreate(): adding FragmentChartList to " + TAG);
				FragmentChartList chartListFragment = new FragmentChartList();
				fragmentTransaction.replace(mChartListLayout.getId(), chartListFragment, FragmentChartList.class.getName());
			}

			mChartPagerLayout = (ViewGroup) v.findViewById(R.id.chart_selector_pager_container);
			if (mChartPagerLayout != null) {
				Log.d("CMS Popularity", TAG + ".onCreate(): adding FragmentChartPager to " + TAG);
				FragmentChartPager chartPagerFragment = new FragmentChartPager();
				fragmentTransaction.replace(mChartPagerLayout.getId(), chartPagerFragment, FragmentChartPager.class.getName());
			}

			fragmentTransaction.commit();
		}

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d("CMS Popularity", TAG + ".onViewCreated()");
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d("CMS Popularity", TAG + ".onActivityCreated()");
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		Log.d("CMS Popularity", TAG + ".onViewStateRestored()");
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.d("CMS Popularity", TAG + ".onStart()");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d("CMS Popularity", TAG + ".onResume()");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d("CMS Popularity", TAG + ".onPause()");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d("CMS Popularity", TAG + ".onSaveInstanceState()");

		outState.putInt(KEY_STATE_CUR_IMAGE_RESOURCE_ID, mCurImageResourceId);
		outState.putInt(KEY_STATE_CUR_IMAGE_POSITION, mCurImagePosition);
		outState.putString(KEY_STATE_CUR_SITENAME, mSiteName);
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d("CMS Popularity", TAG + ".onStop()");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		Log.d("CMS Popularity", TAG + ".onDestroyView()");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("CMS Popularity", TAG + ".onDestroy()");
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		Log.d("CMS Popularity", TAG + ".onCreateOptionsMenu()");

		// Inflate the action bar menu
		inflater.inflate(R.menu.fragment_chart_selector, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d("CMS Popularity", TAG + ".onOptionsItemSelected()");

		switch (item.getItemId()) {
			case R.id.chart_menu_zoom:
				// menu option only provided to phone layouts cause tablet layouts show chart zoom at all times
				Log.d("CMS Popularity", TAG + ".onOptionsItemSelected(): rotate menu item selected");

				FragmentChartZoom chartZoomFragment = FragmentChartZoom.newInstance(mCurImageResourceId, mCurImagePosition);

				Bundle bundle = new Bundle();
				bundle.putSerializable(KEY_STATE_CUR_POSITION, mCurImagePosition);
				bundle.putString(KEY_STATE_CUR_SITENAME, mSiteName);
				chartZoomFragment.setArguments(bundle);

				//this.getChildFragmentManager()
				//		.beginTransaction()
				//		.replace(mChartZoomLayout.getId(), fragmentChartZoom, FragmentChartZoom.class.getName())
				//		.commit();

				// Add the new fragment on top of this one, and add it to the back stack
				this.getFragmentManager()
						.beginTransaction()
						.replace(mChartContainerLayout.getId(), chartZoomFragment, FragmentChartZoom.class.getName())
						.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
						.addToBackStack("FRAGMENT_CHART_ZOOM")  // If string is null fragments overlap!
						.commit();

				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onChartSelected(ChartItem chartItem, int position) {
		Log.d("CMS Popularity", TAG + ".onChartSelected(): title = " + chartItem.getTitle() + " position = " + position);

		// Only inform the other fragments if the selected position is new
		if (mCurImagePosition != position) {
			// Keep track of the selected image
			mCurImageResourceId = chartItem.getImageResId();
			mCurImagePosition = position;

			// Get the fragment manager for this fragment's children
			FragmentManager fragmentManager = getChildFragmentManager();
			FragmentChartList chartListFragment = (FragmentChartList) fragmentManager.findFragmentByTag(FragmentChartList.class.getName());
			FragmentChartPager chartPagerFragment = (FragmentChartPager) fragmentManager.findFragmentByTag(FragmentChartPager.class.getName());

			// If the fragments are in the current layout, have them select the current image
			if (chartListFragment != null) {
				chartListFragment.setChartSelected(chartItem, position);
			}
			if (chartPagerFragment != null) {
				chartPagerFragment.setChartSelected(chartItem, position);
			}

			// Notify the parent listener that an image was selected
			if (mParentOnChartSelectedListener != null) {
				mParentOnChartSelectedListener.onChartSelected(chartItem, position);
			}

		}
	}
}
