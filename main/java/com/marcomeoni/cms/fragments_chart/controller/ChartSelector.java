package com.marcomeoni.cms.fragments_chart.controller;

import com.marcomeoni.cms.fragments_chart.model.ChartItem;

/**
 * Interface to dispatch chart selections.
 *
 * @author Marco Meoni
 */
public interface ChartSelector {

    /**
     * Inform the listener that an image has been selected.
     *
     * @param chartItem
     * @param position
     */
    public void setChartSelected(ChartItem chartItem, int position);
}
