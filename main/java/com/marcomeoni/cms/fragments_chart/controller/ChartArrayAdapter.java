package com.marcomeoni.cms.fragments_chart.controller;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartItem;
import com.marcomeoni.cms.fragments_chart.model.ChartUtils;
import com.marcomeoni.cms.fragments_chart.model.ChartThumbnailCache;

/**
 *
 * @author Marco Meoni
 */
public class ChartArrayAdapter extends ArrayAdapter<ChartItem> {
    private static final String TAG = ChartArrayAdapter.class.getSimpleName();
    /**
     * Static thumbnail cache to live through config changes (rotations)
     */
    public static ChartThumbnailCache mThumbnailCache;
    private final int mLayoutResourceId;
    private Context context;

    /**
     * @param context
     * @param layoutResourceId
     * @param imageItems
     */
    public ChartArrayAdapter(Context context, int layoutResourceId, ChartItem[] imageItems) {
        super(context, layoutResourceId, imageItems);

        this.context = context;
        mLayoutResourceId = layoutResourceId;

        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        int memoryClassBytes = activityManager.getMemoryClass() * 1024 * 1024;
        if (mThumbnailCache == null) {
            mThumbnailCache = new ChartThumbnailCache(memoryClassBytes / 8);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Only load views if the convertView isn't being recycled, otherwise just get the view holder
        final ChartItemViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(mLayoutResourceId, null);

            holder = new ChartItemViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.chart_row_imageview);
            holder.titleText = (TextView) convertView.findViewById(R.id.chart_row_textview);
            convertView.setTag(holder);
        } else {
            holder = (ChartItemViewHolder) convertView.getTag();
        }

        // Pull out the relevant data to bind to the views
        ChartItem imageItem = getItem(position);
        String title = imageItem.getTitle();
        int imageResId = imageItem.getImageResId();

        // Try looking in the memory cache first
        Bitmap bitmapToSet = null;
        Bitmap bitmapInCache = mThumbnailCache.get((long) imageResId);
        if (bitmapInCache != null) {
            bitmapToSet = bitmapInCache;
            Log.d("CMS Popularity", TAG + ".getView(): position = " + position + " - cache hit");
        }
        // Otherwise, load from storage, get a smaller version for memory saving and put in the cache
        else {
            Resources res = getContext().getResources();
            int pixels = res.getDimensionPixelSize(R.dimen.chart_row_item_dimensions);
            Bitmap bitmapSampled = ChartUtils.decodeSampledBitmapFromResource(res, imageResId, pixels, pixels);
            mThumbnailCache.put((long) imageResId, bitmapSampled);
            bitmapToSet = bitmapSampled;

            Log.d("CMS Popularity", TAG + ".getView(): position = " + position + " - cache miss - bitmap.width/height = " + bitmapSampled.getWidth() + "/" + bitmapSampled.getHeight());
        }

        holder.titleText.setText(title);
        holder.imageView.setImageBitmap(bitmapToSet);
        return convertView;
    }

    private class ChartItemViewHolder {
        public ImageView imageView;
        public TextView titleText;
    }

}
