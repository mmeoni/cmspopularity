package com.marcomeoni.cms.fragments_chart.controller;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartItem;
import com.marcomeoni.cms.fragments_chart.model.ChartData;

/**
 * Fragment to show the images in a list and allow selections
 *
 * @author Marco Meoni
 */
public class FragmentChartList extends Fragment implements OnItemClickListener, ChartSelector {
    private static final String TAG = FragmentChartList.class.getSimpleName();

    private ListView mListView;
    private ChartArrayAdapter mChartArrayAdapter;
    private OnChartSelectedListener mParentOnChartSelectedListener;
    private Boolean mInitialCreate;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("CMS Popularity", TAG + ".onAttach()");

        // Check if parent fragment (if there is one) implements the chart selection interface
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null && parentFragment instanceof OnChartSelectedListener) {
            mParentOnChartSelectedListener = (OnChartSelectedListener) parentFragment;
        }
        // Otherwise, check if parent activity implements the chart selection interface
        else if (activity != null && activity instanceof OnChartSelectedListener) {
            mParentOnChartSelectedListener = (OnChartSelectedListener) activity;
        }
        // If neither implements the chart selection callback, warn that selections are being missed
        else if (mParentOnChartSelectedListener == null) {
            Log.d("CMS Popularity", TAG + ".onAttach(): parent fragment/activity doesnt implement OnChartSelectedListener, "
                    + "image selections will not be communicated to other components!");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onCreate(): savedInstanceState " + (savedInstanceState == null ? "==" : "!=") + " null");

        // Track if this is the initial creation of the fragment
        if (savedInstanceState != null) {
            mInitialCreate = false;
        } else {
            mInitialCreate = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("CMS Popularity", TAG + ".onCreateView(): savedInstanceState " + (savedInstanceState == null ? "==" : "!=") + " null");

        // Inflate the fragment main view in the container provided
        View v = inflater.inflate(R.layout.fragment_chart_list, container, false);

        // Setup views
        mListView = (ListView) v.findViewById(R.id.chart_listview);
        mListView.setOnItemClickListener(this);

        // Set an adapter to bind the image items to the list
        mChartArrayAdapter = new ChartArrayAdapter(
                getActivity(),
                R.layout.chart_row_item,
                ChartData.getChartItems());
        mListView.setAdapter(mChartArrayAdapter);

        // Set list view to highlight when an item is pressed
        mListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        // If first creation of fragment, select the default item
        if (mInitialCreate && mChartArrayAdapter.getCount() > 0) {

            // Default the selection to the first item
            mListView.setItemChecked(ChartData.DEFAULT_CHART_INDEX, true);
        }

        // Track that onCreateView has been called at least once since the
        // initial onCreate
        if (mInitialCreate) {
            mInitialCreate = false;
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onViewCreated()");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onActivityCreated()");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onViewStateRestored()");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("CMS Popularity", TAG + ".onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("CMS Popularity", TAG + ".onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("CMS Popularity", TAG + ".onPause()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("CMS Popularity", TAG + ".onSaveInstanceState()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("CMS Popularity", TAG + ".onStop()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("CMS Popularity", TAG + ".onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("CMS Popularity", TAG + ".onDestroy()");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("CMS Popularity", TAG + ".onItemClick(): " + "pos: " + position + " id: " + id);

        // Highlight the selected row
        mListView.setItemChecked(position, true);

        // Inform our parent listener that an image was selected
        if (mParentOnChartSelectedListener != null) {
            ChartItem chartItem = mChartArrayAdapter.getItem(position);
            mParentOnChartSelectedListener.onChartSelected(chartItem, position);
        }
    }

    @Override
    public void setChartSelected(ChartItem chartItem, int position) {
        Log.d("CMS Popularity", TAG + ".setChartSelected(): title = " + chartItem.getTitle() + " position = " + position);
        if (isResumed()) {
            // If the selected position is valid, and different than what is
            // currently selected, highlight that row in the list and scroll to it
            if (position >= 0 && position < mChartArrayAdapter.getCount()
                    && position != mListView.getCheckedItemPosition()) {
                // Highlight the selected row and scroll to it
                mListView.setItemChecked(position, true);
                mListView.smoothScrollToPosition(position);
            }
        }
    }

}
