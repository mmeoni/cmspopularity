package com.marcomeoni.cms.fragments_chart.controller;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartItem;
import com.marcomeoni.cms.fragments_chart.model.ChartData;

/**
 * Fragment to display a selected chart
 *
 * @author Marco Meoni
 */
public class FragmentChartZoom extends Fragment implements ChartSelector {
    private static final String TAG = FragmentChartZoom.class.getSimpleName();

    // Passed in argument keys
    private static final String FULL_CLASSPATH = FragmentChartZoom.class.getName();
    private static final String KEY_ARG_IMAGE_RES_ID = FULL_CLASSPATH + ".KEY_ARG_IMAGE_RES_ID";
    // State keys
    private static final String KEY_STATE_IMAGE_POSITION = "KEY_STATE_IMAGE_POSITION";
    private static final String KEY_STATE_IMAGE_RES_ID = "KEY_STATE_IMAGE_RES_ID";
    private static final String KEY_STATE_CUR_SITENAME = "KEY_STATE_CUR_SITENAME";
    private static final String KEY_STATE_CUR_POSITION = "KEY_STATE_CUR_POSITION";

    private Context context;
    private View mView;
    private ImageView mImageView;
    private static String mSiteName;
    private static int mPosition;
    private int mImageResourceId, mImagePosition;
    private Boolean mInitialCreate;

    /**
     * Convenience method for creating an ImageRotatorFragment without needing to know any bundle keys
     *
     * @param imageResourceId default image resource to show
     * @return a new instance of ImageRotatorFragment with args set in the bundle
     */
    public static FragmentChartZoom newInstance(int imageResourceId, int imagePos) {
        Log.d("CMS Popularity", TAG + ".newInstance(): imageResourceId = " + imageResourceId + ", imagePos = " + imagePos);

        FragmentChartZoom newImageRotatorFragment = new FragmentChartZoom();

        Bundle args = newImageRotatorFragment.getArguments();
        if (args == null) {
            args = new Bundle();
        }
        args.putInt(KEY_ARG_IMAGE_RES_ID, imageResourceId);
        args.putInt(KEY_STATE_IMAGE_POSITION, imagePos);
        args.putString(KEY_STATE_CUR_SITENAME, mSiteName);

        newImageRotatorFragment.setArguments(args);

        return newImageRotatorFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("CMS Popularity", TAG + ".onAttach()");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onCreate(): savedInstanceState " + (savedInstanceState == null ? "==" : "!=") + " null");

        Bundle args = getArguments();
        if (args != null) {
            mImageResourceId = args.getInt(KEY_ARG_IMAGE_RES_ID, -1);
            mImagePosition = 0;

            mSiteName = args.getString(KEY_STATE_CUR_SITENAME);
            mPosition = args.getInt(KEY_STATE_CUR_POSITION);
            Log.d("CMS Popularity", TAG + ".onCreate(). Received request for site " + mSiteName + ", position " + mPosition);
        }
        else {
            mImageResourceId = ChartData.getChartItems()[0].getImageResId();
            mImagePosition = 0;
        }

        if (savedInstanceState != null) {
            mInitialCreate = false;

            mImageResourceId = savedInstanceState.getInt(KEY_STATE_IMAGE_RES_ID);
            mImagePosition = savedInstanceState.getInt(KEY_STATE_IMAGE_POSITION);
            mSiteName = savedInstanceState.getString(KEY_STATE_CUR_SITENAME);
        }
        else {
            mInitialCreate = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        Log.d("CMS Popularity", TAG + ".onCreateView()");

        final View v = inflater.inflate(R.layout.fragment_chart_zoom, container, false);
        mView = v;
        context = getActivity().getApplicationContext();

        // DISMISSED. Previous version displays pictures
        /*
        //mImageView.setImageResource(mImageResourceId);
        mImageView = (ImageView) v.findViewById(R.id.fragment_zoom_image);
        mImageView.setImageBitmap(ChartData.getChartBitmap(mImagePosition));
        */

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onViewCreated()");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onActivityCreated()");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onViewStateRestored()");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("CMS Popularity", TAG + ".onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("CMS Popularity", TAG + ".onResume(). CHART BUILT for the 1st time");

        // Refresh detail chart. markerLabel is used to choose chart type
        TextView markerLabel = (TextView) mView.findViewById(R.id.marker_label);
        markerLabel.setText("Site: " + mSiteName);
        mView = ChartData.getChartView(mView, mSiteName, mPosition, R.string.PREFERENCE_CHART_THRESHOLD_DETAIL, (int) markerLabel.getTextSize());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("CMS Popularity", TAG + ".onPause()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("CMS Popularity", TAG + ".onSaveInstanceState()");

        outState.putInt(KEY_STATE_IMAGE_RES_ID, mImageResourceId);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("CMS Popularity", TAG + ".onStop()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("CMS Popularity", TAG + ".onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("CMS Popularity", TAG + ".onDestroy()");
    }

    @Override
    public void setChartSelected(ChartItem imageItem, int position) {
        Log.d("CMS Popularity", TAG + ".setChartSelected(). CHART REFRESHED. Title = " + imageItem.getTitle() + ", position = " + position);

        // Refresh detail chart. Needed if big screen hosting ZoomFragment
        TextView markerLabel = (TextView) mView.findViewById(R.id.marker_label);
        markerLabel.setText("Site: " + mSiteName);
        mView = ChartData.getChartView(mView, mSiteName, position,  R.string.PREFERENCE_CHART_THRESHOLD_DETAIL, (int) markerLabel.getTextSize());

        /*
        if (isResumed()) {
            mImageResourceId = imageItem.getImageResId();
            //mImageView.setImageResource(mImageResourceId);
            mImageView.setImageBitmap(ChartData.getChartBitmap(position));
        }
        */
    }
}
