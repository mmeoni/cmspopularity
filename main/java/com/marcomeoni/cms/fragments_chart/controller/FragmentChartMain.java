package com.marcomeoni.cms.fragments_chart.controller;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartItem;
import com.marcomeoni.cms.fragments_chart.model.ChartData;

public class FragmentChartMain extends Fragment implements OnChartSelectedListener {
    private static final String TAG = FragmentChartMain.class.getSimpleName();

    // State keys
    private static final String KEY_STATE_CUR_SITENAME = "KEY_STATE_CUR_SITENAME";

    private Context context;
    private Activity activity;
    private ViewGroup mChartSelectorLayout, mChartZoomLayout;
    private String sitename;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("CMS Popularity", TAG + ".onCreate()");
        super.onCreate(savedInstanceState);
        this.activity = getActivity();
        this.context = activity.getApplicationContext();
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString(KEY_STATE_CUR_SITENAME) != null) {
                sitename = bundle.getString(KEY_STATE_CUR_SITENAME);
                Log.d("CMS Popularity", TAG + ".onCreate(). Received request for site " + sitename);

                // Here chart bitmaps are generated
                ChartData.generateChartBitmaps(context, sitename, R.string.PREFERENCE_CHART_THRESHOLD_PREVIEW);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chart_main, container, false);

        // If layout has a container for the chart selector fragment, add it
        Log.d("CMS Popularity", TAG + ".onCreateView(): looking for layout chart_selector_container");
        mChartSelectorLayout = (ViewGroup) v.findViewById(R.id.chart_selector_container);
        if (mChartSelectorLayout != null) {
            Log.d("CMS Popularity", TAG + ".onCreateView(): adding FragmentChartSelector to " + TAG);

            // Add chart selector fragment to the activity's container layout
            FragmentChartSelector fragmentChartSelector = new FragmentChartSelector();

            Bundle bundle = new Bundle();
            bundle.putString(KEY_STATE_CUR_SITENAME, sitename);
            fragmentChartSelector.setArguments(bundle);

            // N.B. getFragmentManager() will return NULL in child fragment calling getParentFragment(), which spoils device rotation
            //      see http://stackoverflow.com/questions/14804526/getparentfragment-returning-null
            //      FragmentTransaction fragmentTransaction = activity.getFragmentManager().beginTransaction();
            this.getChildFragmentManager()
                    .beginTransaction()
                    .replace(mChartSelectorLayout.getId(), fragmentChartSelector, FragmentChartSelector.class.getName())
                    .commit();
        }
        else
            Log.d("CMS Popularity", TAG + ".onCreateView(): chart_selector_container NOT FOUND!");

        // If our layout has a container for the chart zoom fragment, add it
        Log.d("CMS Popularity", TAG + ".onCreateView(): looking for layout chart_zoom_container");
        mChartZoomLayout = (ViewGroup) v.findViewById(R.id.chart_zoom_container);
        if (mChartZoomLayout != null) {
            Log.d("CMS Popularity", TAG + ".onCreateView(): adding FragmentChartZoom to " + TAG);

            // Add chart zoom fragment to the activity's container layout
            FragmentChartZoom fragmentChartZoom = FragmentChartZoom.newInstance(
                    ChartData.getChartItems()[ChartData.DEFAULT_CHART_INDEX].getImageResId(), 0);

            Bundle bundle = new Bundle();
            bundle.putString(KEY_STATE_CUR_SITENAME, sitename);
            fragmentChartZoom.setArguments(bundle);

            this.getChildFragmentManager()
                    .beginTransaction()
                    .replace(mChartZoomLayout.getId(), fragmentChartZoom, FragmentChartZoom.class.getName())
                    .commit();
        }
        else
            Log.d("CMS Popularity", TAG + ".onCreateView(): chart_zoom_container NOT FOUND!");

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("CMS Popularity", TAG + ".onStart()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("CMS Popularity", TAG + ".onSaveInstanceState()");
    }

    @Override
    public void onChartSelected(ChartItem imageItem, int position) {
        Log.d("CMS Popularity", TAG + ".onChartSelected(): title = " + imageItem.getTitle() + " position = " + position);

        FragmentManager fragmentManager = this.getChildFragmentManager();
        FragmentChartZoom fragmentChartZoom = (FragmentChartZoom) fragmentManager.findFragmentByTag(
                FragmentChartZoom.class.getName());

        // If the zoomed fragment is in the current layout, update its selected chart
        if (fragmentChartZoom != null) {
            fragmentChartZoom.setChartSelected(imageItem, position);
        }
    }

}
