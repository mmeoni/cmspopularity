package com.marcomeoni.cms.fragments_chart.controller;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartData;
import com.marcomeoni.cms.fragments_chart.model.ChartItem;

/**
 * Pager adapter that binds chart items to a swiping pager.
 *
 * @author Marco Meoni
 */
public class ChartPagerAdapter extends PagerAdapter {

    private final ChartItem[] mChartItemArray;

    /**
     * Constructor that takes an array of image items to display.
     *
     * @param chartItemArray
     */
    public ChartPagerAdapter(ChartItem[] chartItemArray) {
        mChartItemArray = chartItemArray;
    }

    @Override
    public int getCount() {
        return mChartItemArray.length;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        Context context = collection.getContext();
        Resources res = context.getResources();
        int paddingInPixels = res.getDimensionPixelSize(R.dimen.fragment_pager_chart_padding);

        // Create the image view and set the padding and bitmap
        ImageView imageView = new ImageView(context);
        imageView.setPadding(paddingInPixels, paddingInPixels, paddingInPixels, paddingInPixels);

        //imageView.setImageResource(mChartItemArray[position].getImageResId());
        imageView.setImageBitmap(ChartData.getChartBitmap(position));

        // Add the image view to the collection
        collection.addView(imageView);

        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((ImageView) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mChartItemArray[position].getTitle();
    }

}
