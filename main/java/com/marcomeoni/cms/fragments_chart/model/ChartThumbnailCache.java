package com.marcomeoni.cms.fragments_chart.model;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.util.LruCache;

/**
 * LRU cache implementation for holding thumbnail images in memory instead of loading from storage.
 *
 * @author MArco Meoni
 */
public class ChartThumbnailCache extends LruCache<Long, Bitmap> {

    /**
     * @param maxSizeBytes
     */
    public ChartThumbnailCache(int maxSizeBytes) {
        super(maxSizeBytes);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    protected int sizeOf(Long key, Bitmap value) {
        // Return size of in-memory Bitmap, counted against maxSizeBytes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            return value.getByteCount();
        }
        // Pre HC-MR1
        else {
            return value.getRowBytes() * value.getHeight();
        }
    }
}
