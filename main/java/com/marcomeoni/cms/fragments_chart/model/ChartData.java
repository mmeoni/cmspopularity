package com.marcomeoni.cms.fragments_chart.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.db.Popularity;
import com.marcomeoni.cms.db.PopularityDAO;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Chart data to display
 *
 * @author Marco Meoni
 */
public class ChartData {
    private static final String TAG = ChartData.class.getSimpleName();
    public static final int DEFAULT_CHART_INDEX = 0;

    private static Context mContext;

    private static List<Popularity> mListUsers;
    private static GraphicalView graphicalView;
    private static ChartItem[] mChartItems;
    private static String[] mChartTitles = {"DS Hits", "Popularity", "Accesses", "User Share", "Data Volume", "Machine Learning", "Peak Zones", "Distribution"};
    private static int[] mChartIcons = {R.drawable.ic_chart1, R.drawable.ic_chart2, R.drawable.ic_chart3, R.drawable.ic_chart4, R.drawable.ic_chart5, R.drawable.ic_chart6, R.drawable.ic_chart7, R.drawable.ic_chart8};
    private static Bitmap[] mChartBitmaps = {null, null, null, null, null, null, null, null};

    public static ChartItem[] getChartItems() {
        if (mChartItems == null) {
            mChartItems = new ChartItem[mChartIcons.length];

            for (int i = 0; i < mChartItems.length; i++) {
                ChartItem imageItem = new ChartItem(mChartTitles[i], mChartIcons[i]);
                mChartItems[i] = imageItem;
            }
        }
        return mChartItems;
    }

    // Called from FragmentChartMain. Generates all chart bitmaps for current site
    public static void generateChartBitmaps(Context context, String site, int PREFERENCE_CHART_THRESHOLD) {
        Log.d("CMS Popularity", TAG + ".generateChartBitmaps() for site " + site);
        mContext = context;
        int width = 500, height = 500;

        PopularityDAO popularityDAO = new PopularityDAO(context);

        SharedPreferences sp = context.getSharedPreferences(context.getString(R.string.PREFERENCE_FILENAME), context.MODE_PRIVATE);
        mListUsers = popularityDAO.retrieveNDSbyUser(site, Integer.valueOf(sp.getString(context.getString(PREFERENCE_CHART_THRESHOLD), "20")));

        graphicalView = ChartFactory.getBarChartView(mContext, getChartDataset(mListUsers), getChartRenderer(mListUsers, 0, PREFERENCE_CHART_THRESHOLD, 20), BarChart.Type.DEFAULT);
        mChartBitmaps[0] = ChartUtils.bitmapFromChartView(graphicalView, width, height);

        graphicalView = ChartFactory.getBarChartView(mContext, getChartDataset(mListUsers), getChartRenderer(mListUsers, 1, PREFERENCE_CHART_THRESHOLD, 20), BarChart.Type.STACKED);
        mChartBitmaps[1] = ChartUtils.bitmapFromChartView(graphicalView, width, height);

        graphicalView = ChartFactory.getTimeChartView(mContext, getChartDataset(mListUsers), getChartRenderer(mListUsers, 2, PREFERENCE_CHART_THRESHOLD, 20), "hi");
        mChartBitmaps[2] = ChartUtils.bitmapFromChartView(graphicalView, width, height);

        graphicalView = ChartFactory.getPieChartView(mContext, getChart1CategorySeries(mListUsers), getChart1CategoryRenderer(mListUsers));
        mChartBitmaps[3] = ChartUtils.bitmapFromChartView(graphicalView, width, height);

        graphicalView = ChartFactory.getCubeLineChartView(mContext, getChartDataset(mListUsers), getChartRenderer(mListUsers, 4, PREFERENCE_CHART_THRESHOLD, 20), 1);
        mChartBitmaps[4] = ChartUtils.bitmapFromChartView(graphicalView, width, height);

    }

    public static Bitmap getChartBitmap(int position) {
        Log.d("CMS Popularity", TAG + ".getChartBitmap(): returning bitmap #" + position);
        return mChartBitmaps[position];
    }

    public static View getChartView(View v, String site, int chartType, int PREFERENCE_CHART_THRESHOLD, int textSize) {
        Log.d("CMS Popularity", TAG + ".getChartView(). Chart Type is " + chartType);

        SharedPreferences sp = v.getContext().getSharedPreferences(v.getContext().getString(R.string.PREFERENCE_FILENAME), v.getContext().MODE_PRIVATE);

        PopularityDAO popularityDAO = new PopularityDAO(v.getContext());
        mListUsers = popularityDAO.retrieveNDSbyUser(site, Integer.valueOf(sp.getString(v.getContext().getString(PREFERENCE_CHART_THRESHOLD), "0")));

        LinearLayout layout = (LinearLayout) v.findViewById(R.id.chart);

        switch (chartType) {
            case 0:
                graphicalView = ChartFactory.getBarChartView(v.getContext(), ChartData.getChartDataset(mListUsers), ChartData.getChartRenderer(mListUsers, 0, PREFERENCE_CHART_THRESHOLD, textSize), BarChart.Type.DEFAULT);
                break;
            case 1:
                graphicalView = ChartFactory.getBarChartView(v.getContext(), ChartData.getChartDataset(mListUsers), ChartData.getChartRenderer(mListUsers, 1, PREFERENCE_CHART_THRESHOLD, textSize), BarChart.Type.STACKED);
                break;
            case 2:
                graphicalView = ChartFactory.getTimeChartView(v.getContext(), ChartData.getChartDataset(mListUsers), ChartData.getChartRenderer(mListUsers, 2, PREFERENCE_CHART_THRESHOLD, textSize), "hi");
                break;
            case 3:
                graphicalView = ChartFactory.getPieChartView(v.getContext(), getChart1CategorySeries(mListUsers), getChart1CategoryRenderer(mListUsers));
                break;
            case 4:
                graphicalView = ChartFactory.getCubeLineChartView(v.getContext(), ChartData.getChartDataset(mListUsers), ChartData.getChartRenderer(mListUsers, 4, PREFERENCE_CHART_THRESHOLD, textSize), 1);
                break;
            default:
                graphicalView = ChartFactory.getBarChartView(v.getContext(), ChartData.getChartDataset(mListUsers), ChartData.getChartRenderer(mListUsers, 0, PREFERENCE_CHART_THRESHOLD, textSize), BarChart.Type.DEFAULT);
        }

        layout.removeAllViews();
        layout.addView(graphicalView);

        // Only is non-static ChartData
        // if (graphicalView != null) graphicalView.repaint();

        return v;
    }

    public static XYMultipleSeriesDataset getChartDataset(List<Popularity> listUsers) {
        XYMultipleSeriesDataset ds = new XYMultipleSeriesDataset();
        ArrayList<String> legendTitles = new ArrayList<String>();
        for (int i = 0; i < 1; i++) {
            legendTitles.add("Users");
            CategorySeries series = new CategorySeries(legendTitles.get(i));
            for (Popularity entry : listUsers) {
                series.add(entry.getNDS());
            }
            //XYSeries xyseries=new XYSeries("Series #" + i);
            //ds.addSeries(xyseries);
            ds.addSeries(series.toXYSeries());
        }
        return ds;
    }

    public static XYMultipleSeriesRenderer getChartRenderer(List<Popularity> listUsers, int chartType, int PREFERENCE_CHART_THRESHOLD, int textSize) {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();

        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) renderer.setChartTitle("Data Popularity");
        renderer.setChartTitleTextSize(textSize);

        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) {
            renderer.setMargins(new int[]{textSize * 2, (int) (textSize * 2.5), textSize, textSize * 2});
            //renderer.setYLabels(10);
            renderer.setYLabelsAlign(Paint.Align.RIGHT);
        }
        else
        {
            renderer.setMargins(new int[]{10, 5, 5, 10});
            renderer.setShowLabels(false);
            //renderer.setXLabelsColor(Color.BLACK);
        }

        renderer.setAxisTitleTextSize(textSize);
        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) renderer.setLabelsTextSize(textSize);
        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) renderer.setLabelsColor(Color.WHITE);

        //renderer.setLegendTextSize(textSize);
        //renderer.setLegendHeight(textSize * 4);
        renderer.setShowLegend(false);

        //renderer.setXTitle("User");
        //renderer.setXAxisMin(0.5);
        //renderer.setXAxisMax(10.5);

        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) renderer.setYTitle("Dataset Hits");
        renderer.setYAxisMin(0);
        renderer.setYAxisMax(Collections.max(listUsers).getNDS() + 1);

        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) {
            renderer.setXLabels(0); // sets the number labels to appear
            //renderer.setXLabelsAlign(Paint.Align.LEFT);
            renderer.setXLabelsAngle(-30.0f);
        }

        renderer.setBarSpacing(0.5);

        renderer.setShowGrid(true);
        renderer.setGridColor(Color.GRAY);
        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) {
            renderer.setPanEnabled(true, false);
            renderer.setZoomEnabled(true, false);
            renderer.setZoomButtonsVisible(true);
            renderer.setClickEnabled(true);
        }

        switch (chartType) {
            case 0:
                SimpleSeriesRenderer r1 = new SimpleSeriesRenderer();
                r1.setColor(Color.RED);
                if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) {
                    r1.setChartValuesTextSize((int) (textSize * 0.75));
                    r1.setDisplayChartValues(true);
                }
                renderer.addSeriesRenderer(r1);
                break;
            case 1:
            default:
                XYSeriesRenderer r = new XYSeriesRenderer();
                r.setColor(Color.CYAN);
                renderer.addSeriesRenderer(r);
                break;
        }

        if (PREFERENCE_CHART_THRESHOLD == R.string.PREFERENCE_CHART_THRESHOLD_DETAIL) {
            int i = 0;
            for (Popularity entry : listUsers)
                renderer.addXTextLabel(++i, entry.getUsername());
        }

        return renderer;
    }

    public static CategorySeries getChart1CategorySeries(List<Popularity> listUsers) {
        CategorySeries series = new CategorySeries("Pie");

        for (Popularity entry : listUsers) {
            series.add(entry.getUsername(), entry.getNDS());
        }

        return series;
    }

    public static DefaultRenderer getChart1CategoryRenderer(List<Popularity> listUsers) {
        DefaultRenderer renderer = new DefaultRenderer();

        for (Popularity entry : listUsers) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            renderer.addSeriesRenderer(r);
        }

        return renderer;
    }

}
