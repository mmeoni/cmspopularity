package com.marcomeoni.cms.fragments_chart.model;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;
import android.view.View;

import com.marcomeoni.cms.R;

/**
 * Utility class for images/icons manipulations.
 *
 * @author Marco Meoni
 */
public class ChartUtils {
    private static final String TAG = ChartUtils.class.getSimpleName();

    public static int getMarkerIcon(String siteName) {
        String country;
        country = siteName.substring(siteName.lastIndexOf('.') + 1);
        Log.d("CMS Popularity", TAG + ".getMarkerIcon('" + country + "')");

        // TODO: improve and make it dynamic
        if (country.equals("br"))
            return R.drawable.ic_flag_br;
        else if (country.equals("edu"))
            return R.drawable.ic_flag_edu;
        else if (country.equals("org"))
            return R.drawable.ic_flag_org;
        else if (country.equals("gov"))
            return R.drawable.ic_flag_gov;
        else if (country.equals("ca"))
            return R.drawable.ic_flag_ca;
        else if (country.equals("us"))
            return R.drawable.ic_flag_us;
        else if (country.equals("uk"))
            return R.drawable.ic_flag_uk;
        else if (country.equals("pt"))
            return R.drawable.ic_flag_pt;
        else if (country.equals("es"))
            return R.drawable.ic_flag_es;
        else if (country.equals("be"))
            return R.drawable.ic_flag_be;
        else if (country.equals("fr"))
            return R.drawable.ic_flag_fr;
        else if (country.equals("ch"))
            return R.drawable.ic_flag_ch;
        else if (country.equals("at"))
            return R.drawable.ic_flag_at;
        else if (country.equals("de"))
            return R.drawable.ic_flag_de;
        else if (country.equals("it"))
            return R.drawable.ic_flag_it;
        else if (country.equals("hr"))
            return R.drawable.ic_flag_hr;
        else if (country.equals("hu"))
            return R.drawable.ic_flag_hu;
        else if (country.equals("ee"))
            return R.drawable.ic_flag_ee;
        else if (country.equals("fi"))
            return R.drawable.ic_flag_fi;
        else if (country.equals("ua"))
            return R.drawable.ic_flag_ua;
        else if (country.equals("kr"))
            return R.drawable.ic_flag_kr;
        else
            return R.drawable.ic_flag_00;
    }

    public static Bitmap bitmapFromChartView(View v, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        v.layout(0, 0, width, height);
        v.draw(c);
        return bitmap;
    }

    /**
     * Load a bitmap equal to or larger than requested dimensions, loading the
     * smallest version into memory.
     *
     * @param resources
     * @param resourceId
     * @param reqWidth
     * @param reqHeight
     * @return bitmap sampled at equal to or larger than the requested dimensions
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources resources, int resourceId, int reqWidth, int reqHeight) {

        // First decode check the raw image dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, resourceId, options);

        // Calculate the factor to scale down by depending on the desired height
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inScaled = false;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeResource(resources, resourceId, options);
        return bm;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int imageHeight = options.outHeight;
        final int imageWidth = options.outWidth;

        // Calculate the factor to scale down by depending on the desired height
        int inSampleSize = 1;
        if (imageHeight > reqHeight || imageWidth > reqWidth) {

            final int heightRatio = imageHeight / reqHeight;
            final int widthRatio = imageWidth / reqWidth;

            // Choose the smallest factor to scale down by, so the scaled image is always slightly larger than needed
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

		/*
        Log.i(TAG, "calculateInSampleSize: imageHeight = " + imageHeight + " imageWidth = " + imageWidth
				+ " reqWidth = " + reqWidth + " reqHeight = " + reqHeight
				+ " inSampleSize = " + inSampleSize);
		 */

        return inSampleSize;
    }
}
