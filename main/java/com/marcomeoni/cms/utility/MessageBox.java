package com.marcomeoni.cms.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class MessageBox {

    public static void Ok(Activity activity, String title, String msg, int icon) {

        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue ...
                    }
                })
                .setIcon(icon)
                .show();
    }
}


