package com.marcomeoni.cms.utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import com.marcomeoni.cms.db.Popularity;

public class PopularityHM {
    private static final String TAG = PopularityHM.class.getSimpleName();
    private HashMap<String, HashMap<String, HashMap<String, Values>>> hm
            = new HashMap<String, HashMap<String, HashMap<String, Values>>>();

    public PopularityHM(InputStream inputStream, String inputFormat, String inputType, Activity activity) {
        int benchmark = 5;
        BufferedReader reader = null;
        String line = "";
        String[] aLine = new String[7];
        List<String> lLine = new ArrayList<String>();
        Pattern splitSpace = null;

        /*
        benchmark == 1: fastRead
                     2: readChar
                     3: readLine + split
                     4: readLine + pattern
                     5: readLine + indexOf
         */

        try {
            switch (inputFormat) {
                case "CSV":

                    // Get Memory info
                    //ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
                    //ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                    //activityManager.getMemoryInfo(memoryInfo);
                    //long startMem = (memoryInfo.availMem);
                    long startTime = System.nanoTime();

                    switch (benchmark) {
                        case 1:
                            reader = new BufferedReader(new InputStreamReader(inputStream), 1024 * 1024);
                            char[] buffer = new char[1024 * 1024];
                            while (reader.read(buffer) > 0) ;
                            break;
                        case 2:
                            reader = new BufferedReader(new InputStreamReader(inputStream));
                            int col=0;
                            int chr;
                            StringBuilder token = new StringBuilder();

                            while ((chr = reader.read()) != -1) {
                                if ((char) chr == ',' || chr == '\n') {
                                    aLine[col] = token.toString();
                                    col++;
                                    token.setLength(0);
                                    if (chr == '\n') col=0;
                                }
                                else token.append((char)chr);
                            }

                            break;
                        default:
                            reader = new BufferedReader(new InputStreamReader(inputStream));
                            if (benchmark == 4) splitSpace = Pattern.compile(",");

                            while ((line = reader.readLine()) != null) {
                                switch (benchmark) {
                                    case 3:
                                        aLine = line.split(",");
                                        break;
                                    case 4:
                                        aLine = splitSpace.split(line, 0);
                                        break;
                                    case 5:
                                        //lLine.clear();
                                        int pos = 0, end;
                                        int i = 0;
                                        while ((end = line.indexOf(',', pos)) >= 0) {
                                            //lLine.add(line.substring(pos, end));
                                            aLine[i] = line.substring(pos, end);
                                            i++;
                                            pos = end + 1;
                                        }
                                        aLine[i] = line.substring(pos);
                                        break;
                                }

                                int offset = 0;
                                String date = aLine[0];
                                String site = "cern.ch";
                                switch (inputType) {
                                    case "byUserEOS":
                                    break;
                                case "byUserAAA":
                                    site = aLine[1];
                                    offset = 1;
                                    break;
                                }

                                String username = aLine[1 + offset];
                                String ds = aLine[2 + offset];
                                int nHits = Integer.parseInt(aLine[3 + offset]);
                                int nSecs = Integer.parseInt(aLine[4 + offset]);
                                int nKBs = (int) (Long.parseLong(aLine[5 + offset]) / 1024);

                                //this.add(date, site, username, ds, nHits, nSecs, nKBs);
                            }
                            break;
                        }

                    //String ts = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
                    //activityManager.getMemoryInfo(memoryInfo);
                    //long memUsed = startMem - (memoryInfo.availMem);

                    long memUsed = getUsedMemorySize();
                    long duration = System.nanoTime() - startTime;
                    Log.d("CMS Popularity", TAG + ". BM" + benchmark + " ENDED. Duration:" + duration / 1e9 + "secs, memUsed:" + memUsed);

                    break;
                case "JSON":
                    break;
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading " + inputFormat + " stream: " + ex);
        }
        finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException("Error while closing input stream: " + e);
            }
        }
    }

    public long getUsedMemorySize() {
        long freeSize = 0L;
        long totalSize = 0L;
        long usedSize = -1L;
        try {
            Runtime info = Runtime.getRuntime();
            freeSize = info.freeMemory();
            totalSize = info.totalMemory();
            usedSize = totalSize - freeSize;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usedSize;
    }

    private void add(String date, String site, String username, String ds, int nHits, int nSecs, int nKBs) {
        if (!hm.containsKey(site)) {
            hm.put(site, new HashMap<String, HashMap<String, Values>>());
            hm.get(site).put(date, new HashMap<String, Values>());
            hm.get(site).get(date).put(username, new Values(nHits, nSecs, nKBs));
        }
        else {
            if (!hm.get(site).containsKey(date)) {
                hm.get(site).put(date, new HashMap<String, Values>());
                hm.get(site).get(date).put(username, new Values(nHits, nSecs, nKBs));
            }
            else {
                if (!hm.get(site).get(date).containsKey(username)) {
                    hm.get(site).get(date).put(username, new Values(nHits, nSecs, nKBs));
                }
                else {
                    Values values = hm.get(site).get(date).get(username);
                    values.update(nHits, nSecs, nKBs);
                    hm.get(site).get(date).put(username, values);
                }
            }
        }
    }

    public List<Popularity> getPopularity() {
        List<Popularity> list = new ArrayList<>();

        for (HashMap.Entry<String, HashMap<String, HashMap<String, Values>>> sites : hm.entrySet()) {
            for (HashMap.Entry<String, HashMap<String, Values>> dates : sites.getValue().entrySet()) {
                for (HashMap.Entry<String, Values> usernames : dates.getValue().entrySet()) {
                    list.add(new Popularity(sites.getKey(),
                                    dates.getKey(),
                                    usernames.getKey(),
                                    usernames.getValue().getnDS(),
                                    usernames.getValue().getnHits(),
                                    usernames.getValue().getnSecs(),
                                    usernames.getValue().getnKBs() / 1024)
                    );
                }
            }
        }

        return list;
    }

    public HashMap<String, Long> getSiteLoad() {
        HashMap<String, Long> list = new HashMap<>();

        for (HashMap.Entry<String, HashMap<String, HashMap<String, Values>>> sites : hm.entrySet()) {
            long nEntries = 0;
            for (HashMap.Entry<String, HashMap<String, Values>> dates : sites.getValue().entrySet()) {
                nEntries += dates.getValue().size();
            }
            list.put(sites.getKey(), nEntries);
        }

        return list;
    }

    private class Values {
        private int nDS, nHits, nSecs, nKBs;

        public Values() {
            this(0, 0, 0);
        }

        public Values(int nHits, int nSecs, int nKBs) {
            this.nDS = 1;  // #entries per site/date/user in log file
            this.nHits = nHits;  // #accesses per site/date/user in log file
            this.nSecs = nSecs;  // #seconds per site/date/user in log file
            this.nKBs = nKBs;  // #KBs per site/date/user in log file
        }

        public void update(int nHits, int nSecs, int nKBs) {
            this.nDS++;
            this.nHits += nHits;
            this.nSecs += nSecs;
            this.nKBs += nKBs;
        }

        public int getnDS() {
            return nDS;
        }

        public int getnHits() {
            return nHits;
        }

        public int getnSecs() {
            return nSecs;
        }

        public int getnKBs() {
            return nKBs;
        }
    }
}
