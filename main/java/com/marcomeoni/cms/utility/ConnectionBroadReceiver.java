package com.marcomeoni.cms.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

public class ConnectionBroadReceiver extends BroadcastReceiver {
    private static final String TAG = ConnectionBroadReceiver.class.getSimpleName();

    /*
    @Override
    public void onReceive(Context context, Intent intent) {

        final ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo netInfo = cm.getActiveNetworkInfo();

        final NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isAvailable() || mobile.isAvailable()) {
            MainActivity.net_wifi = wifi.isAvailable();
            MainActivity.net_cellular = mobile.isAvailable();
        }
        else {
            Toast.makeText(context, "Not connected to Internet", Toast.LENGTH_LONG).show();
        }
    }
    */

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("CMS Popularity", TAG + ".onReceive()");

        if (isConnected(context))
            Toast.makeText(context, "Connected", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, "Connection lost", Toast.LENGTH_LONG).show();
    }

    public boolean isConnected(Context context) {
        Log.d("CMS Popularity", TAG + ".isConnected()");
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isAvailable());   // .isConnected()) ??
    }

    public int networkActiveType(Context context) {
        int netType = 0;
        /*
          0 = no active network
          1 = ethernet
          2 = wifi
          3 = cellular
         */

        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo;

        netInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
        if (netInfo != null && netInfo.isConnected())
            netType = 1;
        else {
            netInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (netInfo != null && netInfo.isConnected())
                netType = 2;
            else {
                netInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if (netInfo != null && netInfo.isConnected())
                    netType = 3;
            }
        }

        return netType;
    }

}