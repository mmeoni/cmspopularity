package com.marcomeoni.cms.utility;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.marcomeoni.cms.R;
import com.marcomeoni.cms.db.Dns;
import com.marcomeoni.cms.db.DnsDAO;
import com.marcomeoni.cms.db.PopularityDAO;
import com.marcomeoni.cms.fragments_chart.controller.FragmentChartMain;
import com.marcomeoni.cms.fragments_chart.model.ChartData;
import com.marcomeoni.cms.fragments_chart.model.ChartUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

public class CMSMapView {
    private static final String TAG = CMSMapView.class.getSimpleName();

    // State keys
    private static final String KEY_STATE_CUR_SITENAME = "KEY_STATE_CUR_SITENAME";

    DnsDAO dnsDAO = null;
    PopularityDAO popularityDAO = null;
    ArrayList<String> listSites = null;
    LatLngBounds ITALY = new LatLngBounds(new LatLng(36.4483, 6.6255), new LatLng(47.0919, 18.5216));
    LatLng CERN = new LatLng(46.2324, 6.0502);
    LatLng PISA = new LatLng(43.7167, 10.4000);
    private Context context;
    private Activity activity;
    private GoogleMap gMap;
    private LocationManager locationManager;
    private String mSiteName = "";
    private int zoomLevel = 4, minDS;
    private float opacity = 0.8f;

    private HashMap<Marker, CMSMapMarker> hmMarkers = new HashMap<Marker, CMSMapMarker>();

    public CMSMapView(Context context, Activity activity, GoogleMap googleMap, int selectedDrawerIndex) {
        this.context = context;
        this.activity = activity;
        this.gMap = googleMap;
        this.dnsDAO = new DnsDAO(context);
        this.popularityDAO = new PopularityDAO(context);

        Toast.makeText(context, "Retrieving data from local DB...", Toast.LENGTH_SHORT).show();
        listSites = popularityDAO.retrieveSites();

        switch (selectedDrawerIndex) {
            case 1:
                gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case 2:
                gMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                HashMap<String, Float> sitesMBS = popularityDAO.retrieveMBSbySite();

                ArrayList<Dns> sitesDNS = dnsDAO.retrieveAll();
                Log.d("CMS Popularity", TAG + ".constructor. sitesDNS.Size():" + sitesDNS.size());
                for (Dns site: sitesDNS) {
                    int polyLineWidth = (int) (32 * sitesMBS.get(site.getSite()) / popularityDAO.getMaxMBS());
                    Log.d("CMS Popularity", TAG + ".constructor. Polyline for <" + site.getSite() + ":" + sitesMBS.get(site.getSite()) + ">. Width:" + polyLineWidth + " (" + sitesMBS.get(site.getSite()) + ":" + popularityDAO.getMaxMBS() + ")");

                    PolylineOptions polylineOptions = new PolylineOptions()
                            .add(CERN)
                            .add(new LatLng(site.getLat(), site.getLng()))
                            .width(polyLineWidth)
                            .color(Color.BLUE)
                            .geodesic(true);
                    Polyline polyline = gMap.addPolyline(polylineOptions);
                }
                break;

            /*
            // gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(ITALY, 0));
            // Move the camera instantly to CERN with a zoom of 15.
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CERN, 15));
            // Zoom in, animating the camera.
            gMap.animateCamera(CameraUpdateFactory.zoomIn());
            // Zoom out to zoom level 10, animating with a duration of 2 seconds
            gMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            // Construct a CameraPosition focusing on CERN and animate the camera to that position
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(CERN)   // Sets the center of the map to CERN
                    .zoom(17)       // Sets the zoom
                    .bearing(90)    // Sets the orientation of the camera to east
                    .tilt(30)       // Sets the tilt of the camera to 30 degrees
                    .build();       // Creates a CameraPosition from the builder
            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            */

            // Instantiates a new CircleOptions object and defines the center and radius
            /*
            CircleOptions circleOptions = new CircleOptions()
                    .center(CERN).radius(1000)
                    .strokeColor(Color.GREEN); // In meters
            // Get back the mutable Circle
            Circle circle = gMap.addCircle(circleOptions);
            */
        }

        setUpMap(gMap);

        // get LatLng asynchronously for each site and display marker
        Log.d("CMS Popularity", TAG + ".constructor. Run SiteLatLngAsync() on " + listSites.size() + " sites...");
        for (String site : listSites) {
            new SiteLatLngAsync(site).execute(site);
        }
    }

    private void setUpMap(GoogleMap gMap) {
        if (gMap != null) {
            gMap.setMyLocationEnabled(true);

            // Alternativa a GoogleMapOptions nel costruttore di GoogleMap
            UiSettings mapUI = gMap.getUiSettings();
            mapUI.setCompassEnabled(true);
            mapUI.setZoomGesturesEnabled(true);
            mapUI.setZoomControlsEnabled(true);
            mapUI.setMapToolbarEnabled(true);
            mapUI.setRotateGesturesEnabled(false);
            mapUI.setScrollGesturesEnabled(true);

            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(CERN, zoomLevel));

            gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {
                    marker.showInfoWindow();
                    return true;
                }
            });
        } else
            MessageBox.Ok(activity, "Map Error", "Unable to create Map!", android.R.drawable.ic_dialog_alert);
    }

    private void plotMarker(CMSMapMarker marker) {
        // Create user marker with custom icon and other options
        final MarkerOptions markerOption = new MarkerOptions()
                .title(context.getString(R.string.markertitle))
                .position(new LatLng(marker.getSiteLat(), marker.getSiteLng()))
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));  // bypassed by HUE_GREEN

        final Marker currentMarker = gMap.addMarker(markerOption);
        currentMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        hmMarkers.put(currentMarker, marker);

        gMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());

        gMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Log.d("CMS Popularity", TAG + ".setOnInfoWindowClickListener()");
                Fragment fragment = new FragmentChartMain();
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STATE_CUR_SITENAME, mSiteName);
                fragment.setArguments(bundle);

                if (fragment != null) {
                    activity.getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack("FRAGMENT_CHART_MAIN")  // If string is null fragments overlap!
                            .commit();
                }
            }
        });
    }


    private LatLng JSONParser(String json) {
        Log.d("CMS Popularity", TAG + ".JSONParser(): " + json);
        double lat = 0;
        double lon = 0;

        JSONObject jsonResponse;
        try {
            // Creates a new JSONObject with name/value mappings from the JSON string
            jsonResponse = new JSONObject(json);

            // only 1 line so use the simplified version
            lat = Double.parseDouble(jsonResponse.optString("lat"));
            lon = Double.parseDouble(jsonResponse.optString("lon"));

            /*
            // if JSONArray then returns the value mapped by name
            JSONArray jsonMainNode = jsonResponse.optJSONArray("rootObject");
            int lengthJsonArr = jsonMainNode.length();
            for(int i=0; i < lengthJsonArr; i++)
            {
                // Get Object for each JSON node
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                // Fetch node values
                String lat = jsonChildNode.optString("lat").toString();
                String lon = jsonChildNode.optString("lon").toString();
                OutputData += "Lat: " + lat + ", Lon: " + lon;
            }
            */

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new LatLng(lat, lon);
    }

    // Inner Class that processes HTTP requests
    private class SiteLatLngAsync extends AsyncTask<String, Void, LatLng> {
        String site = "";

        public SiteLatLngAsync(String site) {
            super();
            this.site = site;
        }

        protected LatLng doInBackground(String... sites) {
            LatLng ll = null;

            if (sites.length == 1) {
                site = sites[0];

                Log.d("CMS Popularity", TAG + ".SiteLatLngAsync.doInBackground(). Retrieving IP address for site " + site);
                Dns dns = dnsDAO.retrieve(site);

                // site's info not found in DB. Query the Internet
                if (dns == null)
                    try {
                        // NOTE: To prevent error android.os.StrictMode$AndroidBlockGuardPolicy.onNetwork
                        if (android.os.Build.VERSION.SDK_INT > 9) {
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                        }

                        String ip = InetAddress.getByName(site).getHostAddress();

                        Log.d("CMS Popularity", TAG + ".SiteLatLngAsync.doInBackground(): " + site + " -> " + ip + ". Retrieving Lat/Lng (10secs timeout)");
                        try {
                            // NOTE: supposed to return only 1 JSON line
                            URL url = new URL("http://ip-api.com/json/" + ip);
                            URLConnection conn = url.openConnection();
                            conn.setConnectTimeout(10000);
                            conn.setReadTimeout(10000);
                            conn.setDoOutput(true);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                            String aLine;
                            if ((aLine = reader.readLine()) != null) {
                                ll = JSONParser(aLine);
                                dnsDAO.insert(new Dns(site, ip, ll.latitude, ll.longitude));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("CMS Popularity", TAG + ".SiteLatLngAsync.doInBackground(): failed to retrieve Lat/Lng for " + site);
                        }
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                        Log.d("CMS Popularity", TAG + ".SiteLatLngAsync.doInBackground(): failed to retrieve IP address for " + site);
                    }
                else {
                    ll = new LatLng(dns.getLat(), dns.getLng());
                    Log.d("CMS Popularity", TAG + ".SiteLatLngAsync.doInBackground(): Found DB entry for site " + site + ": {" + dns.getIp() + ", " + dns.getLat() + "/" + dns.getLng() + "}");
                }
            }

            return ll;
        }

        protected void onPostExecute(LatLng ll) {
            if (ll != null) {
                Log.d("CMS Popularity", TAG + ".SiteLatLngAsync.onPostExecute(). Lat/Lng for site " + site + " is: " + ll.latitude + "/" + ll.longitude);
                plotMarker(new CMSMapMarker(site, ll.latitude, ll.longitude));
            } else
                Log.d("CMS Popularity", TAG + ".SiteLatLngAsync.onPostExecute(). Failed to retrieve Lat/Lng for site " + site);
        }

    }

    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        public MarkerInfoWindowAdapter() {
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v = activity.getLayoutInflater().inflate(R.layout.infowindow_layout, null);
            TextView markerLabel = (TextView) v.findViewById(R.id.marker_label);
            TextView markerSubLabel = (TextView) v.findViewById(R.id.marker_sublabel);
            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);

            v.setLayoutParams(new RelativeLayout.LayoutParams(500, 800)); //RelativeLayout.LayoutParams.WRAP_CONTENT));

            CMSMapMarker CMSmarker = hmMarkers.get(marker);
            mSiteName = CMSmarker.getSiteName();
            markerIcon.setImageResource(ChartUtils.getMarkerIcon(CMSmarker.getSiteName()));
            markerLabel.setText(mSiteName);

            SharedPreferences sp = context.getSharedPreferences(context.getString(R.string.PREFERENCE_FILENAME), context.MODE_PRIVATE);
            minDS = Integer.valueOf(sp.getString(context.getString(R.string.PREFERENCE_CHART_THRESHOLD_PREVIEW), "20"));
            markerSubLabel.setText("Threshold: " + minDS + " hits");
            v = ChartData.getChartView(v, mSiteName, 0, R.string.PREFERENCE_CHART_THRESHOLD_PREVIEW, (int) markerLabel.getTextSize());
            return v;
        }
    }

}
