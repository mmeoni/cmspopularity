package com.marcomeoni.cms.utility;

public class CMSMapMarker {
    private String mSiteName;
    private Double mSiteLat;
    private Double mSiteLng;

    public CMSMapMarker(String siteName, Double siteLat, Double siteLng) {
        this.mSiteName = siteName;
        this.mSiteLat = siteLat;
        this.mSiteLng = siteLng;
    }

    public String getSiteName() {
        return mSiteName;
    }

    public void setSiteName(String mSiteName) {
        this.mSiteName = mSiteName;
    }

    public Double getSiteLat() {
        return mSiteLat;
    }

    public void setSiteLat(Double mSiteLat) {
        this.mSiteLat = mSiteLat;
    }

    public Double getSiteLng() {
        return mSiteLng;
    }

    public void setSiteLng(Double mSiteLng) {
        this.mSiteLng = mSiteLng;
    }
}
