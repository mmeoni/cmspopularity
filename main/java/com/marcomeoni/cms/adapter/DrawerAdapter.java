package com.marcomeoni.cms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcomeoni.cms.R;

import java.util.List;

public class DrawerAdapter extends BaseAdapter {
    Context context;
    List<DrawerItem> menuItems;

    public DrawerAdapter(Context context, List<DrawerItem> menuItems) {
        // call super()??
        this.context = context;
        this.menuItems = menuItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // Q: better if inflater is instantiated in the constructor?
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item_drawer, null);
        }
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.drawer_icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.drawer_title);

        DrawerItem menuItem = menuItems.get(position);

        imgIcon.setImageResource(menuItem.getIcon());
        txtTitle.setText(menuItem.getTitle());
        return convertView;
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    @Override
    public Object getItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return menuItems.indexOf(getItem(position));
    }
}