package com.marcomeoni.cms.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.db.Site;
import com.marcomeoni.cms.db.SiteDAO;

import java.util.List;

public class SiteAdapter extends ArrayAdapter<Site> {
    List<Site> sites;
    private Context context;

    public SiteAdapter(Context context, List<Site> sites) {
        super(context, R.layout.list_item_site, sites);
        this.context = context;
        this.sites = sites;
    }

    @Override
    public int getCount() {
        return sites.size();
    }

    @Override
    public Site getItem(int position) {
        return sites.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("CMS Popularity", "SiteAdapter.getView()");

        Site site;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_site, null);
        }

        final ViewHolder viewholder = new ViewHolder();
        viewholder.siteIdTxt = (TextView) convertView.findViewById(R.id.txt_site_id);
        viewholder.siteNameTxt = (TextView) convertView.findViewById(R.id.txt_site_url);
        viewholder.siteTypeSpi = (Spinner) convertView.findViewById(R.id.spin_site_type);
        viewholder.siteActiveChk = (CheckBox) convertView.findViewById(R.id.chk_site_active);

        viewholder.siteActiveChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Site checkedsite = (Site) viewholder.siteActiveChk.getTag();
                checkedsite.setActive(buttonView.isChecked() ? 1 : 0);
                (new SiteDAO(context)).update(checkedsite);
            }
        });

        site = sites.get(position);

        // bind current Site to checkbox for later update, if necessary
        viewholder.siteActiveChk.setTag(site);

        //viewholder.siteActiveChk.setTag(sites.get(position));
        //convertView.setTag(viewholder);
        // ((ViewHolder) convertView.getTag()).siteActiveChk.setTag(sites.get(position));
        // ViewHolder holder = (ViewHolder) convertView.getTag();

        //viewholder.siteIdTxt.setText(site.getId() + ") ");
        viewholder.siteNameTxt.setText(site.getUrl());
        viewholder.siteTypeSpi.setSelection(site.getType());
        viewholder.siteActiveChk.setChecked(site.getActive() == 1);
        viewholder.siteTypeSpi.setEnabled(false);

        Log.d("CMS Popularity", "SiteAdapter.getView(). Site is now " + site.toString());

        return convertView;
    }

    @Override
    public void add(Site site) {
        sites.add(site);
        notifyDataSetChanged();
        super.add(site);
    }

    @Override
    public void remove(Site site) {
        sites.remove(site);
        notifyDataSetChanged();
        super.remove(site);
    }

    private class ViewHolder {
        TextView siteIdTxt;
        TextView siteNameTxt;
        Spinner siteTypeSpi;
        CheckBox siteActiveChk;
    }
}