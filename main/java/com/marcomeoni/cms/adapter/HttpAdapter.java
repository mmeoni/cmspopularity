package com.marcomeoni.cms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.fragments_chart.model.ChartUtils;

import java.util.List;

public class HttpAdapter extends ArrayAdapter<HttpItem> {
    private static final String TAG = HttpAdapter.class.getSimpleName();
    private Context context;
    private List<HttpItem> items;

    public HttpAdapter(Context context, List<HttpItem> items) {
        super(context, R.layout.list_item_http, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public HttpItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getSiteId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Log.d("CMS Popularity", TAG + ".getView()");

        HttpItem item;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_http, null);
        }

        final ViewHolder viewholder = new ViewHolder();
        viewholder.itemIcon = (ImageView) convertView.findViewById(R.id.http_siteIcon);
        viewholder.itemName = (TextView) convertView.findViewById(R.id.http_siteName);
        viewholder.itemLoad = (TextView) convertView.findViewById(R.id.http_siteLoad);

        item = items.get(position);

        viewholder.itemIcon.setImageResource(ChartUtils.getMarkerIcon(item.getSiteName()));
        viewholder.itemName.setText(item.getSiteName());
        viewholder.itemLoad.setText(Long.toString(item.getSiteLoad()));

        return convertView;
    }

    @Override
    public void add(HttpItem item) {
        items.add(item);
        notifyDataSetChanged();
        super.add(item);
    }

    @Override
    public void remove(HttpItem item) {
        items.remove(item);
        notifyDataSetChanged();
        super.remove(item);
    }

    private class ViewHolder {
        ImageView itemIcon;
        TextView itemName;
        TextView itemLoad;
    }

}