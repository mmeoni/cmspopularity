package com.marcomeoni.cms.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

// Singletone pattern at http://www.androiddesignpatterns.com/2012/05/correctly-managing-your-sqlite-database.html
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "popularity";

    // Table MV_XRD
    public static final String TABLE_MV_XRD = "MV_XRD_Users";
    public static final String COLUMN_MV_XRD_SITE = "site";
    public static final String COLUMN_MV_XRD_DATE = "date";
    public static final String COLUMN_MV_XRD_USERNAME = "username";
    public static final String COLUMN_MV_XRD_DSNAME = "dsname";
    public static final String COLUMN_MV_XRD_NDS = "nDS";
    public static final String COLUMN_MV_XRD_NHITS = "nHits";
    public static final String COLUMN_MV_XRD_NSECS = "nSecs";
    public static final String COLUMN_MV_XRD_NKBS = "nBytes";
    // Table SITES
    public static final String TABLE_SITES = "tbl_sites";
    public static final String COLUMN_SITES_ID = "id";
    public static final String COLUMN_SITES_URL = "url";
    public static final String COLUMN_SITES_TYPE = "type";
    public static final String COLUMN_SITES_ACTIVE = "active";
    // Table DNS
    public static final String TABLE_DNS = "tbl_dns";
    public static final String COLUMN_DNS_SITE = "site";
    public static final String COLUMN_DNS_IP = "ip";
    public static final String COLUMN_DNS_LAT = "lat";
    public static final String COLUMN_DNS_LNG = "lng";

    private static final String CREATE_TABLE_MV_XRD =
            "CREATE TABLE IF NOT EXISTS " + TABLE_MV_XRD + " (" +
                    COLUMN_MV_XRD_SITE + " VARCHAR, " +
                    COLUMN_MV_XRD_DATE + " VARCHAR, " +
                    COLUMN_MV_XRD_USERNAME + " VARCHAR, " +
                    COLUMN_MV_XRD_NDS + " INT(3), " +
                    COLUMN_MV_XRD_NHITS + " INT(8), " +
                    COLUMN_MV_XRD_NSECS + " INT(8), " +
                    COLUMN_MV_XRD_NKBS + " INT(8)," +
                    " PRIMARY KEY(" + COLUMN_MV_XRD_SITE + "," + COLUMN_MV_XRD_DATE + "," + COLUMN_MV_XRD_USERNAME + "))";
    private static final String CREATE_TABLE_SITES =
            "CREATE TABLE IF NOT EXISTS " + TABLE_SITES + "(" +
                    COLUMN_SITES_ID + " INTEGER PRIMARY KEY, " +
                    COLUMN_SITES_URL + " TEXT, " +
                    COLUMN_SITES_ACTIVE + " INTEGER, " +
                    COLUMN_SITES_TYPE + " INTEGER" + ")";
    private static final String INSERT_TABLE_SITES_1 =
            "INSERT INTO " + TABLE_SITES + "(" + COLUMN_SITES_URL + ", " + COLUMN_SITES_ACTIVE + ", " + COLUMN_SITES_TYPE + ") VALUES ('http://cern.ch/mmeoni/CMS/part-r-00000', 1, 1)";
    private static final String INSERT_TABLE_SITES_2 =
            "INSERT INTO " + TABLE_SITES + "(" + COLUMN_SITES_URL + ", " + COLUMN_SITES_ACTIVE + ", " + COLUMN_SITES_TYPE + ") VALUES ('http://cern.ch/mmeoni/CMS/part-r-00001', 1, 1)";
    private static final String INSERT_TABLE_SITES_3 =
            "INSERT INTO " + TABLE_SITES + "(" + COLUMN_SITES_URL + ", " + COLUMN_SITES_ACTIVE + ", " + COLUMN_SITES_TYPE + ") VALUES ('http://46.37.26.114/CMS/part-r-00001', 0, 1)";
    private static final String CREATE_TABLE_DNS =
            "CREATE TABLE IF NOT EXISTS " + TABLE_DNS + "(" +
                    COLUMN_DNS_SITE + " TEXT PRIMARY KEY, " +
                    COLUMN_DNS_IP + " TEXT, " +
                    COLUMN_DNS_LAT + " REAL, " +
                    COLUMN_DNS_LNG + " REAL" + ")";
    /*
    private static final String INSERT_TABLE_DNS =
            "INSERT INTO " + TABLE_DNS + "(" +
                    COLUMN_DNS_SITE + ", " +
                    COLUMN_DNS_IP + ", " +
                    COLUMN_DNS_LAT + ", " +
                    COLUMN_DNS_LNG + ") VALUES ('cern.ch', '188.184.9.234', 6, 6)";
    */

    private static DBHelper sInstance;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBHelper getInstance(Context context) {
        // Use application context to dont accidentally leak on Activity's context
        // See bit.ly/6LRzfx
        if (sInstance == null) {
            Log.d("CMS Popularity", "DBHelper.getInstance().");
            sInstance = new DBHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public static synchronized void flushMVData(SQLiteDatabase db) {
        Log.d("CMS Popularity", "DBHelper.flushData()");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MV_XRD);
        db.execSQL(CREATE_TABLE_MV_XRD);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        Log.d("CMS Popularity", "DBHelper.onOpen().");
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("CMS Popularity", "DBHelper.onCreate(). CREATE tables {" + TABLE_SITES + ", " + TABLE_DNS + ", " + TABLE_MV_XRD + "}");
        db.execSQL(CREATE_TABLE_SITES);
        db.execSQL(CREATE_TABLE_DNS);
        db.execSQL(CREATE_TABLE_MV_XRD);
        Log.d("CMS Popularity", "DBHelper.onCreate(). Adding 3 pre-defined Site URLs");
        db.execSQL(INSERT_TABLE_SITES_1);
        db.execSQL(INSERT_TABLE_SITES_2);
        db.execSQL(INSERT_TABLE_SITES_3);
        //Log.d("CMS Popularity", "DBHelper.onCreate(). Filling DNS: " + INSERT_TABLE_DNS);
        //db.execSQL(INSERT_TABLE_DNS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("CMS Popularity", "DBHelper.onUpgrade(). Version is " + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SITES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DNS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MV_XRD);
        onCreate(db);
    }
}
