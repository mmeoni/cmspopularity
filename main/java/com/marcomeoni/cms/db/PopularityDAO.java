package com.marcomeoni.cms.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class PopularityDAO {
    private static final String TAG = PopularityDAO.class.getSimpleName();
    
    private static final String WHERE_KEYS_EQUAL = DBHelper.COLUMN_MV_XRD_SITE + " =?";
    protected SQLiteDatabase database;
    private DBHelper dbHelper;
    private Context context;
    private float maxMBS = 0;

    public PopularityDAO(Context context) {
        this.context = context;
        dbHelper = DBHelper.getInstance(context);
        database = dbHelper.getWritableDatabase();
    }

    public long insert(Popularity user) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_MV_XRD_SITE, user.getSite());
        values.put(DBHelper.COLUMN_MV_XRD_DATE, user.getDate());
        values.put(DBHelper.COLUMN_MV_XRD_USERNAME, user.getUsername());
        values.put(DBHelper.COLUMN_MV_XRD_NDS, user.getNDS());
        values.put(DBHelper.COLUMN_MV_XRD_NHITS, user.getNHits());
        values.put(DBHelper.COLUMN_MV_XRD_NSECS, user.getNSecs());
        values.put(DBHelper.COLUMN_MV_XRD_NKBS, user.getnKBs());

        long result = -1; // SQL errors return -1
        try {
            result = database.insert(DBHelper.TABLE_MV_XRD, null, values);
        }
        catch (Exception e) {
        }
        //Log.d("CMS Popularity", TAG + ".insert() " + user.toString() + ". rc: " + result);
        return result;
    }

    // TODO: where clause to be corrected
    public long update(Popularity user) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_MV_XRD_SITE, user.getSite());
        values.put(DBHelper.COLUMN_MV_XRD_DATE, user.getDate());
        values.put(DBHelper.COLUMN_MV_XRD_USERNAME, user.getUsername());
        values.put(DBHelper.COLUMN_MV_XRD_NDS, user.getNDS());
        values.put(DBHelper.COLUMN_MV_XRD_NHITS, user.getNHits());
        values.put(DBHelper.COLUMN_MV_XRD_NSECS, user.getNSecs());
        values.put(DBHelper.COLUMN_MV_XRD_NKBS, user.getnKBs());

        long result = database.update(DBHelper.TABLE_SITES, values, WHERE_KEYS_EQUAL, new String[]{String.valueOf(user.getSite())});
        Log.d("CMS Popularity", TAG + ".update() rc: " + result);
        return result;
    }

    public void flushMVData() {
        DBHelper.flushMVData(database);
    }

    public ArrayList<String> retrieveSites() {
        ArrayList<String> sites = new ArrayList<String>();
        Log.d("CMS Popularity", TAG + ".retrieveSites().");

        Cursor cursor = database.query(
                DBHelper.TABLE_MV_XRD,
                new String[]{DBHelper.COLUMN_MV_XRD_SITE},
                null,
                null,
                DBHelper.COLUMN_MV_XRD_SITE,
                null, null);

        while (cursor.moveToNext()) {
            sites.add(cursor.getString(0));
        }

        return sites;
    }

    public ArrayList<Popularity> retrieveNDSbyUser(String site, int minDS) {
        ArrayList<Popularity> users = new ArrayList<Popularity>();
        Log.d("CMS Popularity", TAG + ".retrieveNDSbyUser()");
        // TODO: instead, would it be better to use database = getReadableDatabase() ??
        Cursor cursor = database.query(
                DBHelper.TABLE_MV_XRD,
                new String[]{
                        DBHelper.COLUMN_MV_XRD_USERNAME,
                        "SUM(" + DBHelper.COLUMN_MV_XRD_NDS + ") AS totDS",
                },
                "site = ?",
                new String[]{
                        site
                },
                DBHelper.COLUMN_MV_XRD_USERNAME,
                "totDS > " + minDS, null);

        while (cursor.moveToNext()) {
            Popularity user = new Popularity();
            user.setUsername(cursor.getString(0));
            user.setNDS(cursor.getInt(1));
            users.add(user);
        }

        return users;
    }

    public HashMap<String, Float> retrieveMBSbySite() {
        HashMap<String, Float> hmMBSbySite = new HashMap<>();
        Log.d("CMS Popularity", TAG + ".retrieveMBSbySite()");

        Cursor cursor = database.query(
                DBHelper.TABLE_MV_XRD,
                new String[] {
                        DBHelper.COLUMN_MV_XRD_SITE,
                        "SUM(ABS(" + DBHelper.COLUMN_MV_XRD_NKBS + "))",
                },
                null, null, DBHelper.COLUMN_MV_XRD_SITE, null, null);

        float MBS = 0;
        while (cursor.moveToNext()) {
            MBS = cursor.getFloat(1) / 1024;
            hmMBSbySite.put(cursor.getString(0), MBS);
            if (MBS > maxMBS) maxMBS = MBS;
        }
        return hmMBSbySite;
    }

    public float getMaxMBS() {
        return maxMBS;
    }
}