package com.marcomeoni.cms.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DnsDAO {
    private static final String WHERE_SITE_EQUALS = DBHelper.COLUMN_DNS_SITE + " =?";
    protected SQLiteDatabase database;
    private DBHelper dbHelper;
    private Context mContext;

    public DnsDAO(Context context) {
        this.mContext = context;
        Log.d("CMS Popularity", "DnsDAO. Getting DBHelper instance");
        dbHelper = DBHelper.getInstance(mContext);
        database = dbHelper.getWritableDatabase();
    }

    public long insert(Dns dns) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_DNS_SITE, dns.getSite());
        values.put(DBHelper.COLUMN_DNS_IP, dns.getIp());
        values.put(DBHelper.COLUMN_DNS_LAT, dns.getLat());
        values.put(DBHelper.COLUMN_DNS_LNG, dns.getLng());

        long result = database.insert(DBHelper.TABLE_DNS, null, values);
        Log.d("CMS Popularity", "DnsDAO.insert(" + dns.toString() + "). rc: " + result);
        return result;
    }

    public Dns retrieve(String site) {
        Dns dns = null;

        Cursor cursor = database.query(
                DBHelper.TABLE_DNS,
                new String[]{
                        DBHelper.COLUMN_DNS_SITE,
                        DBHelper.COLUMN_DNS_IP,
                        DBHelper.COLUMN_DNS_LAT,
                        DBHelper.COLUMN_DNS_LNG
                },
                DBHelper.COLUMN_DNS_SITE + " = ?",
                new String[]{site},
                null, null, null);

        if (cursor.moveToNext()) {
            dns = new Dns(cursor.getString(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3));
        }

        return dns;
    }

    public ArrayList<Dns> retrieveAll() {
        ArrayList<Dns> dnsList = new ArrayList<Dns>();

        Cursor cursor = database.query(
                DBHelper.TABLE_DNS,
                new String[]{
                        DBHelper.COLUMN_DNS_SITE,
                        DBHelper.COLUMN_DNS_IP,
                        DBHelper.COLUMN_DNS_LAT,
                        DBHelper.COLUMN_DNS_LNG
                },
                null,
                null,
                null, null, null);

        while (cursor.moveToNext()) {
            Dns dns = new Dns(cursor.getString(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3));
            dnsList.add(dns);
        }

        return dnsList;
    }

}