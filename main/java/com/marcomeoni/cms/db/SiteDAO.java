package com.marcomeoni.cms.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class SiteDAO {
    private static final String TAG = SiteDAO.class.getSimpleName();

    private static final String WHERE_ID_EQUALS = DBHelper.COLUMN_SITES_ID + " =?";
    protected SQLiteDatabase database;
    private DBHelper dbHelper;
    private Context mContext;

    public SiteDAO(Context context) {
        this.mContext = context;
        Log.d("CMS Popularity", TAG + "(). Get DBHelper instance");
        dbHelper = DBHelper.getInstance(mContext);
        database = dbHelper.getWritableDatabase();
    }

    public long insert(Site site) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_SITES_URL, site.getUrl());
        values.put(DBHelper.COLUMN_SITES_TYPE, site.getType());
        values.put(DBHelper.COLUMN_SITES_ACTIVE, site.getActive());

        long result = database.insert(DBHelper.TABLE_SITES, null, values);
        Log.d("CMS Popularity", TAG + ".insert(). rc = " + result);
        return result;
    }

    public long update(Site site) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_SITES_URL, site.getUrl());
        values.put(DBHelper.COLUMN_SITES_TYPE, site.getType());
        values.put(DBHelper.COLUMN_SITES_ACTIVE, site.getActive());

        long result = database.update(DBHelper.TABLE_SITES, values, WHERE_ID_EQUALS, new String[]{String.valueOf(site.getId())});
        Log.d("CMS Popularity", TAG + ".update(). rc = " + result);
        return result;
    }

    public long delete(Site site) {
        long result = database.delete(DBHelper.TABLE_SITES, WHERE_ID_EQUALS, new String[]{site.getId() + ""});
        Log.d("CMS Popularity", TAG + ".delete(). rc = " + result);
        return result;
    }

    //USING query() method
    public ArrayList<Site> retrieveSites(Boolean all) {
        ArrayList<Site> sites = new ArrayList<Site>();

        Cursor cursor = database.query(DBHelper.TABLE_SITES,
                new String[]{
                        DBHelper.COLUMN_SITES_ID,
                        DBHelper.COLUMN_SITES_URL,
                        DBHelper.COLUMN_SITES_TYPE,
                        DBHelper.COLUMN_SITES_ACTIVE}, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Site site = new Site();
            site.setId(cursor.getInt(0));
            site.setUrl(cursor.getString(1));
            site.setType(cursor.getInt(2));
            site.setActive(cursor.getInt(3));

            if (all)
                sites.add(site);
            else {
                if (site.getActive() == 1)
                    sites.add(site);
            }
        }
        return sites;
    }

    //USING rawQuery() method
    /*public ArrayList<Site> retrieveSites() {
        ArrayList<Site> sites = new ArrayList<Site>();

		String sql = "SELECT " + DBHelper.ID_COLUMN + ","
				+ DBHelper.COLUMN_SITES_URL + ","
				+ DBHelper.COLUMN_SITES_TYPE + ","
				+ DBHelper.COLUMN_SITES_ACTIVE + " FROM "
				+ DBHelper.TABLE_SITES;

		Cursor cursor = database.rawQuery(sql, null);

		while (cursor.moveToNext()) {
			Site site = new Site();
			site.setId(cursor.getInt(0));
			site.setUrl(cursor.getString(1));
			site.setType(cursor.getInt(2)));
			site.setActive(cursor.getInt(3));
			sites.add(site);
		}
		return Sites;
	}*/

    //Retrieves a single Site record with the given id
    public Site retrieveSite(long id) {
        Site site = null;

        String sql = "SELECT * FROM " + DBHelper.TABLE_SITES + " WHERE " + DBHelper.COLUMN_SITES_ID + " = ?";

        Cursor cursor = database.rawQuery(sql, new String[]{id + ""});

        if (cursor.moveToNext()) {
            site = new Site();
            site.setId(cursor.getInt(0));
            site.setUrl(cursor.getString(1));
            site.setType(cursor.getInt(2));
            site.setActive(cursor.getInt(3));
        }
        return site;
    }

}