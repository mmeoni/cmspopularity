package com.marcomeoni.cms.db;

import android.os.Parcel;
import android.os.Parcelable;

public class Dns implements Parcelable {

    public static final Creator<Dns> CREATOR = new Creator<Dns>() {
        public Dns createFromParcel(Parcel in) {
            return new Dns(in);
        }

        public Dns[] newArray(int size) {
            return new Dns[size];
        }
    };
    private String site;
    private String ip;
    private Double lat;
    private Double lng;

    public Dns() {
        super();
    }

    public Dns(String site, String ip, Double lat, Double lng) {
        super();
        this.site = site;
        this.ip = ip;
        this.lat = lat;
        this.lng = lng;
    }

    private Dns(Parcel in) {
        super();
        this.site = in.readString();
        this.ip = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Dns [site=" + site + ", IP=" + ip + ", lat="
                + lat + ", lng=" + lng + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (lat + lng);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Dns other = (Dns) obj;
        return site == other.site;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(getSite());
        parcel.writeString(getIp());
        parcel.writeDouble(getLat());
        parcel.writeDouble(getLng());
    }

}