package com.marcomeoni.cms.db;

import android.os.Parcel;
import android.os.Parcelable;

public class Popularity implements Comparable<Popularity>, Parcelable {
    public static final Parcelable.Creator<Popularity> CREATOR = new Parcelable.Creator<Popularity>() {
        public Popularity createFromParcel(Parcel in) {
            return new Popularity(in);
        }

        public Popularity[] newArray(int size) {
            return new Popularity[size];
        }
    };
    private String site;
    private String date;
    private String username;
    private int nDS;
    private int nHits;
    private int nSecs;
    private int nKBs;

    public Popularity() {
        super();
    }

    private Popularity(Parcel in) {
        super();
        this.site = in.readString();
        this.date = in.readString();
        this.username = in.readString();
        this.nDS = in.readInt();
        this.nHits = in.readInt();
        this.nSecs = in.readInt();
        this.nKBs = in.readInt();
    }

    public Popularity(String site, String date, String username, int nDS, int nHits, int nSecs, int nKBs) {
        this.site = site;
        this.date = date;
        this.username = username;
        this.nDS = nDS;
        this.nHits = nHits;
        this.nSecs = nSecs;
        this.nKBs = nKBs;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getNDS() {
        return nDS;
    }

    public void setNDS(int nDS) {
        this.nDS = nDS;
    }

    public int getNHits() {
        return nHits;
    }

    public void setNHits(int nHits) {
        this.nHits = nHits;
    }

    public int getNSecs() {
        return nSecs;
    }

    public void setnSecs(int nSecs) {
        this.nSecs = nSecs;
    }

    public int getnKBs() {
        return nKBs;
    }

    public void setnKBs(int nKBs) {
        this.nKBs = nKBs;
    }

    @Override
    // TODO: To be corrected.........
    public int compareTo(Popularity another) {
        long anotherNDS = another != null ? another.getNDS() : 0;
        return (int) (nDS - anotherNDS);
    }

    @Override
    // TODO: To be corrected.........
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(site);
        parcel.writeString(date);
        parcel.writeString(username);
        parcel.writeInt(nDS);
        parcel.writeInt(nHits);
        parcel.writeInt(nSecs);
        parcel.writeInt(nKBs);
    }

    @Override
    public String toString() {
        return "<" + site + "," + date + "," + username + ">: {" + nDS + "," + nHits + "," + nSecs + "," + nKBs + "}";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + nDS;
        return result;

        //int result = HashCodeUtil.SEED;
        //result = HashCodeUtil.hash( result, username );
    }

    @Override
    // TODO: To be corrected.........
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Popularity other = (Popularity) obj;
        return username == other.username;
    }

}