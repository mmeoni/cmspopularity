package com.marcomeoni.cms.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.marcomeoni.cms.R;

public class FragmentPreferences extends PreferenceFragment {
    private static final String TAG = FragmentPreferences.class.getSimpleName();

    /*
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager prefMgr = getPreferenceManager();
        prefMgr.setSharedPreferencesName(getActivity().getApplicationContext().getString(R.string.PREFERENCE_FILENAME));
        prefMgr.setSharedPreferencesMode(Context.MODE_PRIVATE);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("CMS Popularity", TAG + ".onCreate()");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d("CMS Popularity", TAG + ".onActivityCreated()");
        super.onActivityCreated(savedInstanceState);

        PreferenceManager prefMgr = getPreferenceManager();
        prefMgr.setSharedPreferencesName(getActivity().getApplicationContext().getString(R.string.PREFERENCE_FILENAME));
        prefMgr.setSharedPreferencesMode(Context.MODE_PRIVATE);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        getView().setClickable(false);
    }

}