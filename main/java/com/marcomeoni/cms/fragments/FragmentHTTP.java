package com.marcomeoni.cms.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.marcomeoni.cms.adapter.HttpAdapter;
import com.marcomeoni.cms.adapter.HttpItem;
import com.marcomeoni.cms.db.Popularity;
import com.marcomeoni.cms.db.PopularityDAO;
import com.marcomeoni.cms.utility.MessageBox;
import com.marcomeoni.cms.utility.PopularityHM;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class FragmentHTTP extends ListFragment {
    private static final String TAG = FragmentHTTP.class.getSimpleName();
    private static final String KEY_STATE_FOO = "KEY_STATE_FOO";

    private Activity activity;
    private AddPopularityTask task;
    private PopularityDAO popularityDAO;

    private HttpAdapter adapter;
    private List<HttpItem> mItems = new ArrayList<>();

    private PopularityHM hmapUsers = null;
    private ArrayList<Popularity> listUsers = null;

    @Override
    public void onAttach(Activity activity) {
        Log.d("CMS Popularity", TAG + ".onAttach()");
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.activity = getActivity();
        Log.d("CMS Popularity", TAG + ".onCreate(): savedInstanceState " + (savedInstanceState == null ? "==" : "!=") + " NULL");

        if (savedInstanceState != null) {
            //mFoo = savedInstanceState.getInt(KEY_STATE_FOO);
        }
        else {
            popularityDAO = new PopularityDAO(activity);
            popularityDAO.flushMVData();

            adapter = new HttpAdapter(activity, mItems);
            this.setListAdapter(adapter);

            Bundle bundle = getArguments();
            if (bundle != null) {
                if (bundle.getSerializable("URLs") != null) {
                    ArrayList<String> serverURLs = (ArrayList<String>) getArguments().getSerializable("URLs");
                    Log.d("CMS Popularity", TAG + ".onCreate(). Received " + serverURLs.size() + " URLs to process");

                    for (String serverURL : serverURLs)
                        new HTTPReader(serverURL).execute(serverURL);
                }
            }
        }
    }

    /*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
    }
    */

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("CMS Popularity", TAG + ".onActivityCreated()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("CMS Popularity", TAG + ".onSaveInstanceState()");

        // Used to handle rotations and do not trigger HTTP requests in onCreate() at each rotation
        // TODO: not enough because MainActivity.onNavigationDrawerItemSelected() creates the fragment again!!
        outState.putInt(KEY_STATE_FOO, 0);
    }

    // Call Chart fragment and pass parameters
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        HttpItem item = (HttpItem) getListAdapter().getItem(position);
        Log.d("CMS Popularity", TAG + ".onListItemClick(). Selected item " + item.getSiteName() + " at position " + position);
    }

    // Inner Class that processes JSON/CSV/WS remote source
    private class HTTPReader extends AsyncTask<String, Void, Void> {
        private List logList;
        private String logError, logType, siteName;
        private ProgressDialog mDialog = new ProgressDialog(activity);

        public HTTPReader(String url) {
            // TODO:
            switch (url) {
                case "http://cern.ch/mmeoni/CMS/part-r-00000":
                    logType = "byUserAAA";
                    break;
                case "http://cern.ch/mmeoni/CMS/part-r-00001":
                    logType = "byUserEOS";
                    break;
                default:
                    logType = "byUserAAA";
                    break;
            }
        }

        protected void onPreExecute() {
            mDialog.setMessage("Retrieving data from remote site...");
            mDialog.show();
            // // Set Request parameter
            // try {
            //   data += "&" + URLEncoder.encode("data", "UTF-8") + "=" + serverText.getText();
            // }
            // catch (UnsupportedEncodingException e) {
            //   e.printStackTrace();
            // }
        }

        // Call after onPreExecute method. Make Post Call To Web Server
        protected Void doInBackground(String... urls) {
            siteName = urls[0];
            Log.d("CMS Popularity", TAG + "-> HTTPReader.doInBackground(). Started for site " + siteName + ", type:" + logType);

            try {
                URL url = new URL(siteName);
                // Send POST data request
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(30000);

                // Send HTTP request to server with additional filters
                //OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                //wr.write( data );
                //wr.flush();

                hmapUsers = new PopularityHM(conn.getInputStream(), "CSV", logType, activity);

            } catch (Exception ex) {
                logError = ex.getMessage();
            }

            Log.d("CMS Popularity", TAG + "-> HTTPReader.doInBackground(). Completed for site " + siteName);
            return null;
        }

        // Call UI elements here
        protected void onPostExecute(Void unused) {

            Log.d("CMS Popularity", TAG + "-> HTTPReader.onPostExecute(). Started for " + siteName);
            if (logError != null) {
                MessageBox.Ok(activity, "HTTPReader.onPostExecute()", "Error for " + siteName + System.getProperty("line.separator") + logError, android.R.drawable.ic_dialog_alert);
            } else {

                // This takes the majority of the time. Is it due to HM sorting or DB write?
                // Create HashMap+List from HTTP response. Old version, now everything is in doInBackground()
                // hmapUsers = new PopularityHM(logList, logType);

                listUsers = (ArrayList) hmapUsers.getPopularity();

                // Build listView items and refresh activity
     //           int i = 0;
     //           // for (Popularity item : listUsers) {
     //           for (HashMap.Entry<String, Long> site : hmapUsers.getSiteLoad().entrySet()) {
     //               mItems.add(new HttpItem(i++, site.getKey(), site.getValue()));
     //           }
     //           adapter.notifyDataSetChanged();

                // Write data to DB
                /*
                int err = 0;
                for (Popularity item : listUsers) {
                    if (popularityDAO.insert(item) == -1) err++;
                }
                if (err > 0)
                    MessageBox.Ok(activity, "DB Insert Error", "Failed to insert " + err + " records", android.R.drawable.ic_dialog_alert);
                */
     //           new AddPopularityTask(activity).execute(listUsers);

            }
            mDialog.dismiss();
            Log.d("CMS Popularity", TAG + "-> HTTPReader.onPostExecute(). Completed for " + siteName);
        }

    }

    // TODO: what the hell is this for? Compare with HTTP Task
    public class AddPopularityTask extends AsyncTask<ArrayList<Popularity>, Void, Void> {

        private final WeakReference<Activity> activityWeakRef;
        private long err = 0;

        public AddPopularityTask(Activity context) {
            this.activityWeakRef = new WeakReference<Activity>(context);
        }

        @Override
        protected Void doInBackground(ArrayList<Popularity>... arg0) {
            for (Popularity item : arg0[0]) {
                if (popularityDAO.insert(item) == -1) err++;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if (activityWeakRef.get() != null && !activityWeakRef.get().isFinishing()) {
                Toast.makeText(activityWeakRef.get(), "Site Saved with " + err + " errors", Toast.LENGTH_LONG).show();
            }
        }
    }
}