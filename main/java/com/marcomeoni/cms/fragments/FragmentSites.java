package com.marcomeoni.cms.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.adapter.SiteAdapter;
import com.marcomeoni.cms.db.Site;
import com.marcomeoni.cms.db.SiteDAO;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class FragmentSites extends Fragment implements OnItemClickListener, OnItemLongClickListener {

    public static final String ARG_ITEM_ID = "Site_list";
    private Activity activity;
    private ListView siteListView;
    private SiteAdapter siteListAdapter;
    private SiteDAO siteDAO;
    private GetSitesAsync task;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("CMS Popularity", "FragmentSites.onCreate()");
        super.onCreate(savedInstanceState);
        activity = getActivity();
        siteDAO = new SiteDAO(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("CMS Popularity", "FragmentSites.onCreateView()");
        View view = inflater.inflate(R.layout.site_fragment_list, container, false);

        siteListView = (ListView) view.findViewById(R.id.list_site);

        task = new GetSitesAsync(activity);
        task.execute((Void) null);

        siteListView.setOnItemClickListener(this);
        siteListView.setOnItemLongClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        Log.d("CMS Popularity", "FragmentSites.onResume()");
        getActivity().setTitle(R.string.app_name);
        getActivity().getActionBar().setTitle(R.string.app_name);
        super.onResume();
    }

    @Override
    public void onItemClick(AdapterView<?> list, View view, int position, long arg3) {
        Site site = (Site) list.getItemAtPosition(position);

        if (site != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(FragmentSitesUpdate.TAG, site);
            FragmentSitesUpdate siteDialog = new FragmentSitesUpdate();
            siteDialog.setArguments(arguments);
            siteDialog.show(getFragmentManager(), FragmentSitesUpdate.TAG);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long rowid) {
        final Site site = (Site) parent.getItemAtPosition(position);

        new AlertDialog.Builder(activity)
                .setTitle("Delete site")
                .setMessage("Confirm deleting site " + site.getUrl() + "?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO: Use AsyncTask to delete from database
                        siteDAO.delete(site);
                        siteListAdapter.remove(site);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_dialer)
                .show();

        return true;
    }

    /*
     * This method (DISMISSED BUT STILL USEFUL AS BEST PRACTICE!) is invoked from
     * MainActivity onFinishDialog() method. It is called from FragmentSitesUpdate
     * when an Site record is updated. This is used for communicating between fragments.
     */
    public void updateView() {
        task = new GetSitesAsync(activity);
        task.execute((Void) null);
    }

    public class GetSitesAsync extends AsyncTask<Void, Void, ArrayList<Site>> {

        private final WeakReference<Activity> activityWeakRef;

        // TODO: does it release the activity while processing in background?
        public GetSitesAsync(Activity context) {
            this.activityWeakRef = new WeakReference<Activity>(context);
        }

        @Override
        protected ArrayList<Site> doInBackground(Void... args) {
            ArrayList<Site> sites = siteDAO.retrieveSites(true);
            Log.d("CMS Popularity", "GetSitesAsync.doInBackground(). Retrieved " + sites.size() + " sites.");
            return sites;
        }

        @Override
        protected void onPostExecute(ArrayList<Site> sites) {
            Log.d("CMS Popularity", "GetSitesAsync.onPostExecute(). Sites: " + sites.toString());
            if (activityWeakRef.get() != null && !activityWeakRef.get().isFinishing()) {
                if (sites != null) {
                    if (sites.size() != 0) {
                        siteListAdapter = new SiteAdapter(activity, sites);
                        siteListView.setAdapter(siteListAdapter);
                    } else {
                        Toast.makeText(activity, "Sites not found", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }
    }

}
