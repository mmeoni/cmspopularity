package com.marcomeoni.cms.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.marcomeoni.cms.R;

@SuppressLint("NewApi")
public class FragmentWebview extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.web_fragment, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        WebView web = (WebView) getView().findViewById(R.id.webview);
        web.loadUrl("https://twiki.cern.ch/twiki/bin/view/ITAnalyticsWorkingGroup/WebHome");
    }

}