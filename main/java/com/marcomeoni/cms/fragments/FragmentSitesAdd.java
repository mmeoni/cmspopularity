package com.marcomeoni.cms.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.db.Site;
import com.marcomeoni.cms.db.SiteDAO;

import java.lang.ref.WeakReference;

public class FragmentSitesAdd extends Fragment implements OnClickListener {

    public static final String ARG_ITEM_ID = "site_add_fragment";
    private Fragment fragment;
    private EditText siteUrlEtxt;
    private Spinner siteTypeSpin;
    private Button addButton;
    private Button resetButton;
    private Site site;
    private SiteDAO siteDAO;
    private AddSiteTask task;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragment = this;
        siteDAO = new SiteDAO(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.site_fragment_add, container, false);

        siteUrlEtxt = (EditText) rootView.findViewById(R.id.etxt_url);
        siteTypeSpin = (Spinner) rootView.findViewById(R.id.spin_type);
        addButton = (Button) rootView.findViewById(R.id.button_add);
        resetButton = (Button) rootView.findViewById(R.id.button_reset);

        addButton.setOnClickListener(this);
        resetButton.setOnClickListener(this);

        // for orientation change
        if (savedInstanceState != null) {
        }

        return rootView;
    }

    @Override
    public void onResume() {
        getActivity().setTitle(R.string.txt_add);
        getActivity().getActionBar().setTitle(R.string.txt_add);
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onClick(View view) {
        if (view == addButton) {
            site = new Site();
            site.setUrl(siteUrlEtxt.getText().toString());
            site.setType(siteTypeSpin.getSelectedItemPosition());
            site.setActive(1);

            task = new AddSiteTask(getActivity());
            task.execute((Void) null);
        } else if (view == resetButton) {
            resetAllFields();
        }
    }

    protected void resetAllFields() {
        siteUrlEtxt.setText("");
        siteTypeSpin.setSelection(1);
    }

    public class AddSiteTask extends AsyncTask<Void, Void, Long> {

        private final WeakReference<Activity> activityWeakRef;

        public AddSiteTask(Activity context) {
            this.activityWeakRef = new WeakReference<Activity>(context);
        }

        @Override
        protected Long doInBackground(Void... arg0) {
            long result = siteDAO.insert(site);
            return result;
        }

        @Override
        protected void onPostExecute(Long result) {
            if (activityWeakRef.get() != null && !activityWeakRef.get().isFinishing()) {
                if (result != -1) {
                    Toast.makeText(activityWeakRef.get(), "Site Saved", Toast.LENGTH_LONG).show();
                    getFragmentManager().beginTransaction().replace(R.id.container, new FragmentSites()).commit();
                }
            }
        }
    }
}
