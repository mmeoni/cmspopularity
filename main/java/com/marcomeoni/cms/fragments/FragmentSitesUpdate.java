package com.marcomeoni.cms.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.marcomeoni.cms.R;
import com.marcomeoni.cms.db.Site;
import com.marcomeoni.cms.db.SiteDAO;
import com.marcomeoni.cms.utility.MessageBox;

public class FragmentSitesUpdate extends DialogFragment {
    public static final String TAG = "FragmentSiteDialog";
    private EditText siteUrlEtxt;
    private Spinner siteTypeSpinn;
    private LinearLayout submitLayout;
    private Site site;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        // site won't be null as it is already checked from the caller, but still...
        site = bundle.getParcelable(TAG);

        if (site == null) {
            return null;
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View customDialogView = inflater.inflate(R.layout.site_fragment_add, null);
        submitLayout = (LinearLayout) customDialogView.findViewById(R.id.layout_submit);
        siteUrlEtxt = (EditText) customDialogView.findViewById(R.id.etxt_url);
        siteTypeSpinn = (Spinner) customDialogView.findViewById(R.id.spin_type);

        submitLayout.setVisibility(View.GONE);
        siteUrlEtxt.setText(site.getUrl());
        siteTypeSpinn.setSelection(site.getType());
        siteTypeSpinn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                site.setType(position);
            }

            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(customDialogView)
                .setTitle(R.string.site_update)
                .setCancelable(false)
                .setPositiveButton(R.string.txt_update, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        site.setUrl(siteUrlEtxt.getText().toString());
                        site.setType(siteTypeSpinn.getSelectedItemPosition());
                        Log.d("CMS Popularity", "Site updated to: " + site.toString());
                        long result = (new SiteDAO(getActivity())).update(site);
                        if (result > 0) {
                            dialog.dismiss();
                            // getActivity().onFinishDialog();
                        } else {
                            MessageBox.Ok(getActivity(), "Update Error", "Unable to update Site", android.R.drawable.ic_dialog_alert);
                        }
                    }
                })
                .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    public interface DialogFragmentListenerSite {
        /* Check notes at FragmentSites.updateView() */
        void onFinishDialog();
    }
}